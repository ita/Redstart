/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_SESSION_BINAURAL_HEADPHONES_DIALOG
#define IW_REDSTART_SESSION_BINAURAL_HEADPHONES_DIALOG

#include <QDialog>
#include <QDir>
#include <QString>
#include <QSettings>
#include <QHash>

namespace Ui
{
	class RedstartDialogSessionBinauralHeadphones;
}

class RedstartSessionBinauralHeadphonesDialog : public QDialog
{
	Q_OBJECT

public:
	explicit RedstartSessionBinauralHeadphonesDialog( QWidget *parent );
	~RedstartSessionBinauralHeadphonesDialog();
	QVariantHash GetCoreConfig() const;
	QString GetSessionName() const;
	bool GetStartImmediately() const;

	private slots:
	void on_pushButton_CreateSession_clicked();
	void on_pushButton_CreateAndStartSession_clicked();
	void on_pushButton_BrowseDemoSound_clicked();
	void on_pushButton_BrowseHRIR_clicked();
	void on_pushButton_inverse_hpir_file_select_clicked();
	void on_checkBox_hpir_auto_filter_length_clicked( bool );

private:
	Ui::RedstartDialogSessionBinauralHeadphones* ui;
	QDir m_oLastBasePath;
	QDir m_oHRIRBasePath;
	QDir m_oInverseHPIRBasePath;
	QDir m_oDemoSoundBasePath;
	QSettings m_qSettings;
	bool m_bStartImmediately;
};

#endif // IW_REDSTART_SESSION_BINAURAL_HEADPHONES_DIALOG
