/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_SESSION_EXPERIMENTAL_TALKTHROUGH_DIALOG
#define IW_REDSTART_SESSION_EXPERIMENTAL_TALKTHROUGH_DIALOG

#include <QDialog>
#include <QDir>
#include <QString>
#include <QSettings>
#include <QHash>

namespace Ui
{
	class RedstartSessionExperimentalTalkthroughDialog;
}

class RedstartSessionExperimentalTalkthroughDialog : public QDialog
{
	Q_OBJECT

public:
	explicit RedstartSessionExperimentalTalkthroughDialog( QWidget *parent );
	~RedstartSessionExperimentalTalkthroughDialog();
	QVariantHash GetCoreConfig() const;
	QString GetSessionName() const;
	bool GetStartImmediately() const;

	private slots:
	void on_pushButton_CreateSession_clicked();
	void on_pushButton_CreateAndStartSession_clicked();
	void on_pushButton_select_demo_sound();
	void NumChannelsChanged( int );

private:
	Ui::RedstartSessionExperimentalTalkthroughDialog* ui;
	QDir m_oLastBasePath;
	QDir m_oHRIRBasePath;
	QDir m_oDemoSoundBasePath;
	QString m_sFileBaseName;
	QSettings m_qSettings;
	bool m_bStartImmediately;
};

#endif // IW_REDSTART_SESSION_EXPERIMENTAL_TALKTHROUGH_DIALOG
