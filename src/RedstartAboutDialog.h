/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_ABOUT_DIALOG
#define IW_REDSTART_ABOUT_DIALOG

#include <QDialog>

namespace Ui
{
	class RedstartAboutDialog;
}

class RedstartAboutDialog : public QDialog
{
	Q_OBJECT

public:
	explicit RedstartAboutDialog( QWidget *parent );
	~RedstartAboutDialog();
private slots:

private:
	Ui::RedstartAboutDialog* ui;
};

#endif // IW_REDSTART_ABOUT_DIALOG
