/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "RedstartRunCirculatingSourceDialog.h"
#include <ui_RedstartRunCirculatingSourceDialog.h>

#include <QFileDialog>
#include <QSettings>
#include <QErrorMessage>

#include "RedstartDefaultCoreConfig.h"

#include <VA.h>
#include <ITANumericUtils.h>

RedstartRunCirculatingSourceDialog::RedstartRunCirculatingSourceDialog( QWidget *parent )
	: QDialog( parent )
	, ui( new Ui::RedstartRunCirculatingSourceDialog )
	, m_pVAInterface( NULL )
{
	ui->setupUi( this );

	QDialog::setWindowTitle( "Run circulating source" );

	QSettings qSettings;
	QString sLastBrowseFolder = qSettings.value( "Redstart/last_browse_folder" ).toString();
	std::string s = sLastBrowseFolder.toStdString();

	if( QDir( sLastBrowseFolder ).exists() )
		m_oLastBasePath.setCurrent( sLastBrowseFolder );

	m_dAzimuthDEG = 0.0f;
	m_dElevationDEG = 0.0f;

	m_qVAUpdateTimer = new QTimer( this );
	m_qVAUpdateTimer->setTimerType( Qt::TimerType::PreciseTimer );

	connect( m_qVAUpdateTimer, SIGNAL( timeout() ), this, SLOT( UpdateScene() ) );
}

RedstartRunCirculatingSourceDialog::~RedstartRunCirculatingSourceDialog()
{
	if( m_oLastBasePath.exists() )
		m_qSettings.setValue( "Redstart/last_browse_folder", m_oLastBasePath.absolutePath() );
	delete ui;
	delete m_qVAUpdateTimer;
}

void RedstartRunCirculatingSourceDialog::SetVAInterface( IVAInterface* pVA )
{
	if( pVA )
		m_pVAInterface = pVA;
}

void RedstartRunCirculatingSourceDialog::Start()
{
	try
	{
		CreateScene();
		SetUIElementsEnabledWhenNotRunning( false );
		StartUpdateTimer();
	}
	catch( CVAException& e )
	{
		QString sErrorMessage = QString( "Could not create circulating source example scene: %1" ).arg( QString::fromStdString( e.ToString() ) );
		QErrorMessage oErrMsg;
		oErrMsg.showMessage( sErrorMessage );
		oErrMsg.exec();
	}
}

void RedstartRunCirculatingSourceDialog::SetUIElementsEnabledWhenNotRunning( bool bYes )
{
	const bool bNo = !bYes;
	ui->pushButton_start->setEnabled( bYes );
	ui->pushButton_stop->setEnabled( bNo );
	ui->pushButton_run->setEnabled( bYes );
	ui->lineEdit_macro_DefaultHRIR->setEnabled( bYes );
	ui->lineEdit_macro_DemoSound->setEnabled( bYes );
	ui->checkBox_folders_as_search_path->setEnabled( bYes );
	ui->doubleSpinBox_timeout->setEnabled( bYes );
}

void RedstartRunCirculatingSourceDialog::Stop()
{
	StopUpdateTimer();
	SetUIElementsEnabledWhenNotRunning( true );
}

void RedstartRunCirculatingSourceDialog::on_pushButton_run_clicked()
{
	Start();
	accept();
}

void RedstartRunCirculatingSourceDialog::on_pushButton_start_clicked()
{
	Start();
}

void RedstartRunCirculatingSourceDialog::on_pushButton_stop_clicked()
{
	Stop();
}

void RedstartRunCirculatingSourceDialog::CreateScene()
{
	if( !m_pVAInterface )
		VA_EXCEPT2( MODAL_ERROR, "No connection to a VA core available" );

	m_sSignalID = m_pVAInterface->CreateSignalSourceBufferFromFile( ui->lineEdit_macro_DemoSound->text().toStdString() ).c_str();
	m_pVAInterface->SetSignalSourceBufferLooping( m_sSignalID.toStdString(), true );

	m_iSourceID = m_pVAInterface->CreateSoundSource( "Redstart C++ circulating sound source" );
	//m_pVAInterface->SetSoundSourcePose( m_iSourceID, VAVec3( 2.0f, 1.7f, 2.0f ), VAQuat( 0.0f, 0.0f, 0.0f, 1.0f ) );

	m_pVAInterface->SetSoundSourceSignalSource( m_iSourceID, m_sSignalID.toStdString() );

	m_iHRIRID = m_pVAInterface->CreateDirectivityFromFile( ui->lineEdit_macro_DefaultHRIR->text().toStdString() );

	m_iReceiverID = m_pVAInterface->CreateSoundReceiver( "Redstart C++ example sound receiver" );
	m_pVAInterface->SetSoundReceiverPose( m_iReceiverID, VAVec3( 0.0f, 1.7f, 0.0f ), VAQuat( 0.0f, 0.0f, 0.0f, 1.0f ) );
	m_pVAInterface->SetSoundReceiverDirectivity( m_iReceiverID, m_iHRIRID );
}

void RedstartRunCirculatingSourceDialog::DeleteScene()
{
	m_pVAInterface->SetSoundSourceSignalSource( m_iSourceID, "" );
	m_pVAInterface->DeleteSoundSource( m_iSourceID );
	m_pVAInterface->DeleteSoundReceiver( m_iReceiverID );
	m_pVAInterface->DeleteDirectivity( m_iHRIRID );
	m_pVAInterface->DeleteSignalSource( m_sSignalID.toStdString() );
}

void RedstartRunCirculatingSourceDialog::UpdateScene()
{
	VAVec3 v3Pos;
	v3Pos.x = -std::sin( grad2rad( m_dAzimuthDEG ) ) * ui->doubleSpinBox_radius->value();
	v3Pos.y = std::sin( grad2rad( m_dElevationDEG ) ) * ui->doubleSpinBox_radius->value();
	v3Pos.z = -std::cos( grad2rad( m_dAzimuthDEG ) ) * ui->doubleSpinBox_radius->value();
	
	VAVec3 v3NewPos;
	try
	{
		m_pVAInterface->SetSoundSourcePosition( m_iSourceID, v3Pos );
		v3NewPos = m_pVAInterface->GetSoundSourcePosition( m_iSourceID );

		// Delayed plaback start so that no part of sentence is missing (source has to be placed in space first)
		if( m_pVAInterface->GetSignalSourceBufferPlaybackState( m_sSignalID.toStdString() ) != IVAInterface::VA_PLAYBACK_STATE_PLAYING )
			m_pVAInterface->SetSignalSourceBufferPlaybackAction( m_sSignalID.toStdString(), IVAInterface::VA_PLAYBACK_ACTION_PLAY );
	}
	catch( CVAException& e )
	{
		Stop();
	}

	ui->doubleSpinBox_azimuth->setValue( m_dAzimuthDEG );
	ui->doubleSpinBox_elevation->setValue( m_dElevationDEG );

	ui->doubleSpinBox_source_pos_x->setValue( v3NewPos.x );
	ui->doubleSpinBox_source_pos_y->setValue( v3NewPos.y );
	ui->doubleSpinBox_source_pos_z->setValue( v3NewPos.z );

	ui->doubleSpinBox_runtime_tracker->setValue( m_dRuntimeSeconds );

	m_dAzimuthDEG += ui->doubleSpinBox_azi_inc->value();
	m_dAzimuthDEG = anglef_proj_0_360_DEG( m_dAzimuthDEG );
	m_dElevationDEG += ui->doubleSpinBox_ele_inc->value();
	m_dElevationDEG = anglef_proj_N90_90_DEG( m_dElevationDEG );

	m_dRuntimeSeconds += ui->doubleSpinBox_timeout->value();

	if( m_dRuntimeSeconds > ui->doubleSpinBox_runtime->value() )
		Stop();
}

void RedstartRunCirculatingSourceDialog::StartUpdateTimer()
{
	const double dTimeout = ui->doubleSpinBox_timeout->value();
	const int iIntervalMS = ceil( dTimeout * 1000.0f );
	m_qVAUpdateTimer->setInterval( iIntervalMS );
	m_qVAUpdateTimer->start();
	m_dRuntimeSeconds = 0.0f;
}

void RedstartRunCirculatingSourceDialog::StopUpdateTimer()
{
	m_qVAUpdateTimer->stop();
	m_dRuntimeSeconds = 0.0f;
	m_dAzimuthDEG = 0.0f;
	m_dElevationDEG = 0.0f;

	try
	{
		DeleteScene();
	}
	catch( CVAException& e )
	{
		// Quietly hide that something went wrong. It is possible, that a reset has been triggered or any
		// object managed by this dialog has been removed by another connection - we do not observer the scene, here.
		return;
	}
}

void RedstartRunCirculatingSourceDialog::on_pushButton_BrowseDemoSound_clicked()
{
	QFileDialog fd;
	fd.setNameFilter( "WAV files (*.wav)" );
	fd.setViewMode( QFileDialog::Detail );

	if( m_oLastBasePath.exists() )
		fd.setDirectory( m_oLastBasePath );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			QFile oFile( sFilePath );
			if( oFile.exists() )
			{
				m_oDemoSoundBasePath = fd.directory();
				m_oLastBasePath = m_oDemoSoundBasePath;
			}
			ui->lineEdit_macro_DemoSound->setText( oFile.fileName() );
		}
	}
}

void RedstartRunCirculatingSourceDialog::on_pushButton_BrowseHRIR_clicked()
{
	QFileDialog fd;
	fd.setNameFilter( "DAFF files (*.daff)" );
	fd.setViewMode( QFileDialog::Detail );

	if( m_oLastBasePath.exists() )
		fd.setDirectory( m_oLastBasePath );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			QFile oFile( sFilePath );
			if( oFile.exists() )
			{
				m_oHRIRBasePath = fd.directory();
				m_oLastBasePath = m_oHRIRBasePath;

			}

			ui->lineEdit_macro_DefaultHRIR->setText( sFilePath );
		}
	}
}
