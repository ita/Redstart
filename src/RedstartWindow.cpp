/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include <QErrorMessage>
#include <QUrl>
#include <QDesktopServices>
#include <QHash>
#include <QShortcut>
#include <QFileDialog>

#include "RedstartWindow.h"
#include <ui_RedstartWindow.h>

#include "RedstartRunSimpleExample.h"
#include "RedstartSessionBinauralHeadphonesDialog.h"
#include "RedstartSessionExperimentalTalkthroughDialog.h"
#include "RedstartSessionImportDialog.h"
#include "RedstartSessionWizardDialog.h"
#include "RedstartUtils.h"
#include "RedstartCoreOutputPlainTextEdit.h"
#include "RedstartRunCirculatingSourceDialog.h"
#include "RedstartAboutDialog.h"

#include <VA.h>
#include <VACore.h>
#include <VANet.h>

#include <ITAAsioInterface.h>
#include <ITAPortaudioInterface.h>
#include <ITAException.h>
#include <ITANumericUtils.h>


RedstartWindow::RedstartWindow( bool bFailSafeMode, bool bAutoStart, bool bSkipConfig, QWidget* pParent )
	: QMainWindow( pParent )
	, ui( new Ui::RedstartWindow )
	, m_pVAInterface( NULL )
{
	ui->setupUi( this );

	ui->statusBar->showMessage( "Welcome to VA." );

	// Shortcuts
	QShortcut* start_b = new QShortcut( this );
	start_b->setKey( Qt::Key_F5 );
	start_b->setContext( Qt::WindowShortcut );
	QShortcut* start_c = new QShortcut( this );
	start_c->setKey( QKeySequence( tr( "CTRL+R" ) ) );
	start_c->setContext( Qt::WindowShortcut );

	connect( start_b, SIGNAL( activated() ), this, SLOT( on_shortcut_start_stop() ) );
	connect( start_c, SIGNAL( activated() ), this, SLOT( on_pushButton_start_clicked() ) );

	m_pVANetServer = IVANetServer::Create();
	m_pVANetClient = IVANetClient::Create();

	ui->comboBox_audio_driver->addItem( "ASIO" );
	ui->comboBox_audio_driver->addItem( "Portaudio" );
	ui->comboBox_audio_driver->addItem( "Virtual" );

	ui->comboBox_audio_iface_sampling_rate->addItem( "44.1 kHz", AudioSamplingRate::FS_44kHz );
	ui->comboBox_audio_iface_sampling_rate->addItem( "48 kHz", AudioSamplingRate::FS_48kHz );
	//ui->comboBox_audio_iface_sampling_rate->addItem( "96 kHz", AudioSamplingRate::FS_96kHz );

	ui->comboBox_audio_iface_buffer_size->addItem( "AUTO", AudioBufferSize::AUTO );

	m_pPathsModel = new QStringListModel( ui->listView_paths );
	ui->listView_paths->setModel( m_pPathsModel );

	m_pMacrosModel = new QStringListModel( ui->listView_macros );
	ui->listView_macros->setModel( m_pMacrosModel );

	m_pNetServerClientsModel = new QStringListModel( ui->listView_server_connected_clients );
	ui->listView_server_connected_clients->setModel( m_pNetServerClientsModel );

	m_iPortaudioDefaultDevice = -1;

	if( !m_qSettings.contains( "first_start" ) || m_qSettings.value( "first_start" ) == true )
	{
		m_qSettings.setValue( "first_start", false );
		ui->comboBox_audio_driver->setCurrentIndex( 1 );
	}

	if( !bSkipConfig )
		LoadConfiguration();

	// Signals & slots
	ConnectSignalsAndSlots();

	ui->menuRun->setEnabled( false );

	if( ui->checkBox_redstart_network_connect_as_client->isChecked() )
		ui->groupBox_redstart_audio_iface->setEnabled( false );

	if( ( ui->checkBox_redstart_auto_start->isChecked() || bAutoStart ) && !bFailSafeMode )
		on_pushButton_start_clicked();

	m_pCirculatingSourceDialog = new RedstartRunCirculatingSourceDialog( this );

	m_qSporadicTimer.setTimerType( Qt::TimerType::VeryCoarseTimer );
	m_qSporadicTimer.setInterval( m_qSettings.value( "sporadic_timeout_interval", 200 ).toInt() );
	m_qSporadicTimer.start();
}

RedstartWindow::~RedstartWindow()
{
	m_qSporadicTimer.stop();

	StoreConfiguration();

	if( m_pVANetServer->IsClientConnected() )
	{
		m_pVANetServer->Finalize();
		delete m_pVAInterface;
	}
	delete m_pVANetServer;

	if( m_pVANetClient->IsConnected() )
		m_pVANetClient->Disconnect();
	delete m_pVANetClient;

	delete m_pPathsModel;
	delete m_pMacrosModel;
}

void RedstartWindow::ConnectSignalsAndSlots()
{
	// Input control
	connect( ui->doubleSpinBox_input_gain, SIGNAL( valueChanged( double ) ), this, SLOT( CoreChangeInputSignalDecibel( double ) ) );
	connect( this, SIGNAL( InputSignalChangedDecibel( double ) ), ui->doubleSpinBox_input_gain, SLOT( setValue( double ) ) );
	connect( ui->dial_core_control_input_gain, SIGNAL( valueChanged( int ) ), this, SLOT( CoreChangeInputSignalDecibel( int ) ) );
	connect( this, SIGNAL( InputSignalChangedDecibel( int ) ), ui->dial_core_control_input_gain, SLOT( setValue( int ) ) );
	connect( this, SIGNAL( InputIsMutedChanged( bool ) ), ui->checkBox_core_control_input_mute, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_core_control_input_mute, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeInputIsMuted( int ) ) );

	// Output control
	connect( ui->doubleSpinBox_output_gain, SIGNAL( valueChanged( double ) ), this, SLOT( CoreChangeOutputSignalDecibel( double ) ) );
	connect( this, SIGNAL( OutputSignalChangedDecibel( double ) ), ui->doubleSpinBox_output_gain, SLOT( setValue( double ) ) );
	connect( ui->dial_core_control_output_gain, SIGNAL( valueChanged( int ) ), this, SLOT( CoreChangeOutputSignalDecibel( int ) ) );
	connect( this, SIGNAL( OutputSignalChangedDecibel( int ) ), ui->dial_core_control_output_gain, SLOT( setValue( int ) ) );
	connect( this, SIGNAL( OutputIsMutedChanged( bool ) ), ui->checkBox_core_control_output_mute, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_core_control_output_mute, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeOutputIsMuted( int ) ) );

	// Auralization modes
	connect( this, SIGNAL( GlobalAuralizationModeChanged( int ) ), this, SLOT( CoreChangeGlobalAuralizationMode( int ) ) );
	connect( this, SIGNAL( AuralizationModeDSChanged( bool ) ), ui->checkBox_am_ds, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_ds, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeDS( int ) ) );
	connect( this, SIGNAL( AuralizationModeERChanged( bool ) ), ui->checkBox_am_er, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_er, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeER( int ) ) );
	connect( this, SIGNAL( AuralizationModeDDChanged( bool ) ), ui->checkBox_am_dd, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_dd, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeDD( int ) ) );
	connect( this, SIGNAL( AuralizationModeDIRChanged( bool ) ), ui->checkBox_am_dir, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_dir, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeDIR( int ) ) );
	connect( this, SIGNAL( AuralizationModeDPChanged( bool ) ), ui->checkBox_am_dp, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_dp, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeDP( int ) ) );
	connect( this, SIGNAL( AuralizationModeSLChanged( bool ) ), ui->checkBox_am_sl, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_sl, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeSL( int ) ) );
	connect( this, SIGNAL( AuralizationModeDFChanged( bool ) ), ui->checkBox_am_dif, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_dif, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeDF( int ) ) );
	connect( this, SIGNAL( AuralizationModeSCChanged( bool ) ), ui->checkBox_am_sca, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_sca, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeSC( int ) ) );
	connect( this, SIGNAL( AuralizationModeNFChanged( bool ) ), ui->checkBox_am_nf, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_nf, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeNF( int ) ) );
	connect( this, SIGNAL( AuralizationModeTVChanged( bool ) ), ui->checkBox_am_tv, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_tv, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeTV( int ) ) );
	connect( this, SIGNAL( AuralizationModeMAChanged( bool ) ), ui->checkBox_am_ma, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_ma, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeMA( int ) ) );
	connect( this, SIGNAL( AuralizationModeABChanged( bool ) ), ui->checkBox_am_ab, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_ab, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeAB( int ) ) );
	connect( this, SIGNAL( AuralizationModeTRChanged( bool ) ), ui->checkBox_am_tr, SLOT( setChecked( bool ) ) );
	connect( ui->checkBox_am_tr, SIGNAL( stateChanged( int ) ), this, SLOT( CoreChangeAuralizationModeTR( int ) ) );

	connect( ui->comboBox_core_settings_log_level, SIGNAL( currentIndexChanged( int ) ), this, SLOT( CoreChangeLogLevel( int ) ) );

	connect( &m_qSporadicTimer, SIGNAL( timeout() ), this, SLOT( SporadicTimeout() ) );
	connect( &m_qSporadicTimer, SIGNAL( timeout() ), ui->graphicsView_input_levels, SLOT( UpdateLevels() ) );
	connect( &m_qSporadicTimer, SIGNAL( timeout() ), ui->graphicsView_output_levels, SLOT( UpdateLevels() ) );

}

void RedstartWindow::LoadConfiguration()
{
	RestoreWindowSize();

	bool bValOK;
	int iAudioBackend = m_qSettings.value( "Redstart/audio/backend_idx" ).toInt( &bValOK );
	if( !bValOK )
		iAudioBackend = AudioBackend::ASIO;
	ui->comboBox_audio_driver->setCurrentIndex( iAudioBackend );

	const int iBufferSizeIndex = m_qSettings.value( "Redstart/audio/buffersize_idx" ).toInt( &bValOK );
	if( bValOK && iBufferSizeIndex < ui->comboBox_audio_iface_buffer_size->count() )
		ui->comboBox_audio_iface_buffer_size->setCurrentIndex( iBufferSizeIndex );

	const int iSamplingRateIndex = m_qSettings.value( "Redstart/audio/fs_idx" ).toInt( &bValOK );
	if( bValOK && iSamplingRateIndex < ui->comboBox_audio_iface_sampling_rate->count() )
		ui->comboBox_audio_iface_sampling_rate->setCurrentIndex( iSamplingRateIndex );

	if( m_qSettings.contains( "Redstart/network/address" ) )
		ui->lineEdit_redstart_network_address->setText( m_qSettings.value( "Redstart/network/address" ).toString() );
	if( m_qSettings.contains( "Redstart/network/port" ) )
		ui->lineEdit_redstart_network_address->setText( m_qSettings.value( "Redstart/network/port" ).toString() );
	ui->checkBox_redstart_network_connect_as_client->setChecked( m_qSettings.value( "Redstart/network/client_mode" ).toBool() );

	if( m_qSettings.contains( "Redstart/autostart" ) )
	{
		bool bAutostart = m_qSettings.value( "Redstart/autostart" ).toBool();
		ui->checkBox_redstart_auto_start->setChecked( bAutostart );
	}

	m_iPortaudioDefaultDevice = m_qSettings.value( "Redstart/audio/portaudio_default_device", -1 ).toInt();

	ui->checkBox_portaudio_default_device->setChecked( m_qSettings.value( "Redstart/audio/portaudio_use_default_device", true ).toBool() );

	QVariantList voAudioDevices;
	voAudioDevices = m_qSettings.value( "Redstart/audio/devicelist" ).toList();

	for( const QVariant oAudioDevice : voAudioDevices )
		m_voAudioDevices.push_back( CAudioDeviceSpecs( oAudioDevice.toHash() ) );

	PopulateAudioDevices();

	int iLastDevice = m_qSettings.value( "Redstart/audio/current_device", 0 ).toInt( &bValOK );
	if( bValOK && ui->comboBox_audio_iface_device->count() >= iLastDevice )
		ui->comboBox_audio_iface_device->setCurrentIndex( iLastDevice );

	// Core control
	ui->comboBox_core_settings_log_level->addItem( QString::fromStdString( m_pVAInterface->GetLogLevelStr( IVAInterface::VA_LOG_LEVEL_QUIET ) ) );
	ui->comboBox_core_settings_log_level->addItem( QString::fromStdString( m_pVAInterface->GetLogLevelStr( IVAInterface::VA_LOG_LEVEL_ERROR ) ) );
	ui->comboBox_core_settings_log_level->addItem( QString::fromStdString( m_pVAInterface->GetLogLevelStr( IVAInterface::VA_LOG_LEVEL_WARNING ) ) );
	ui->comboBox_core_settings_log_level->addItem( QString::fromStdString( m_pVAInterface->GetLogLevelStr( IVAInterface::VA_LOG_LEVEL_INFO ) ) );
	ui->comboBox_core_settings_log_level->addItem( QString::fromStdString( m_pVAInterface->GetLogLevelStr( IVAInterface::VA_LOG_LEVEL_VERBOSE ) ) );
	ui->comboBox_core_settings_log_level->addItem( QString::fromStdString( m_pVAInterface->GetLogLevelStr( IVAInterface::VA_LOG_LEVEL_TRACE ) ) );

	if( ui->listView_redstart_session_list->GetCurrentSessionID().isEmpty() == false )
		ui->treeView_session_details->SetStruct( ui->listView_redstart_session_list->GetCurrentConfig() );
}

void RedstartWindow::StoreConfiguration()
{
	m_qSettings.setValue( "Redstart/gui/WindowGeometry", saveGeometry() );
	m_qSettings.setValue( "Redstart/gui/WindowState", saveState() );

	m_qSettings.setValue( "Redstart/audio/backend_idx", ui->comboBox_audio_driver->currentIndex() );

	m_qSettings.setValue( "Redstart/audio/buffersize_idx", ui->comboBox_audio_iface_buffer_size->currentIndex() );
	m_qSettings.setValue( "Redstart/audio/fs_idx", ui->comboBox_audio_iface_sampling_rate->currentIndex() );

	m_qSettings.setValue( "Redstart/network/address", ui->lineEdit_redstart_network_address->text() );

	bool bValOK;
	int iPort = ui->lineEdit_redstart_network_address->text().toInt( &bValOK );
	if( bValOK )
		m_qSettings.setValue( "Redstart/network/port", iPort );

	m_qSettings.setValue( "Redstart/network/client_mode", ui->checkBox_redstart_network_connect_as_client->isChecked() );

	bool bAutostart = ui->checkBox_redstart_auto_start->isChecked();
	m_qSettings.setValue( "Redstart/autostart", bAutostart );

	m_qSettings.setValue( "Redstart/audio/portaudio_default_device", m_iPortaudioDefaultDevice );
	int iLastDevice = ui->comboBox_audio_iface_device->currentIndex();
	m_qSettings.setValue( "Redstart/audio/current_device", iLastDevice );

	m_qSettings.setValue( "Redstart/audio/portaudio_use_default_device", ui->checkBox_portaudio_default_device->isChecked() );

	QVariantList voAudioDevices;
	for( const CAudioDeviceSpecs& oDevice : m_voAudioDevices )
		voAudioDevices.push_back( oDevice.asHash() );

	m_qSettings.setValue( "Redstart/audio/devicelist", voAudioDevices );

	m_qSettings.setValue( "sporadic_timeout_interval", m_qSporadicTimer.interval() );
}

void RedstartWindow::on_actionQuit_triggered()
{
	close();
}

void RedstartWindow::PostCoreStart()
{
	ui->pushButton_start->setEnabled( false );
	ui->pushButton_stop->setEnabled( true );

	// Switch UI elements enabled
	ui->groupBox_redstart_audio_iface->setEnabled( false );
	ui->groupBox_redstart_network->setEnabled( false );
	ui->groupBox_session_list->setEnabled( false );
	ui->groupBox_session_details->setEnabled( false );
	ui->groupBox_core->setEnabled( true );
	ui->groupBox_paths->setEnabled( true );
	ui->groupBox_macros->setEnabled( true );
	ui->menuRun->setEnabled( true );
	ui->menuNew_session->setEnabled( false );
	ui->groupBox_core_control->setEnabled( true );
	ui->graphicsView_input_levels->setEnabled( true );
	ui->graphicsView_output_levels->setEnabled( true );
	ui->groupBox_global_am->setEnabled( true );
	ui->groupBox_clients->setEnabled( true );

	CVAVersionInfo oVersion;
	m_pVAInterface->GetVersionInfo( &oVersion );
	ui->lineEdit_core_settings_version->setText( QString::fromStdString( oVersion.ToString() ) );

	CVAStruct oArgs;
	oArgs[ "getloglevel" ] = true;
	oArgs[ "getmacros" ] = true;
	const CVAStruct oReturn = m_pVAInterface->CallModule( "VACore", oArgs );
	const int iCurrentLogLevel = oReturn[ "loglevel" ];
	ui->comboBox_core_settings_log_level->setCurrentIndex( iCurrentLogLevel );
	ui->comboBox_core_settings_log_level->setCurrentIndex( IVAInterface::VA_LOG_LEVEL_WARNING ); // override until problem with Qt line edit is fixed

	CVAStruct oSearchPaths = m_pVAInterface->GetSearchPaths();
	QStringList sPathList;
	CVAStruct::const_iterator cit = oSearchPaths.Begin();
	while( cit != oSearchPaths.End() )
	{
		const CVAStructValue& oValue( cit++->second );
		if( oValue.IsString() )
			sPathList << std::string( oValue ).c_str();
	}

	m_pPathsModel->setStringList( sPathList );

	if( oReturn.HasKey( "macros" ) )
	{
		CVAStruct oMacros = oReturn[ "macros" ];
		QStringList sMacrosList;
		CVAStruct::const_iterator cit = oMacros.Begin();
		while( cit != oMacros.End() )
		{
			std::string sName( cit->first );
			const CVAStructValue& oValue( cit++->second );
			if( oValue.IsString() )
				sMacrosList << ( sName + " = " + std::string( oValue ) ).c_str();
		}

		m_pMacrosModel->setStringList( sMacrosList );
	}
}

void RedstartWindow::PostCoreStop()
{
	ui->pushButton_start->setEnabled( true );
	ui->pushButton_stop->setEnabled( false );

	// Switch UI elements enabled
	ui->groupBox_redstart_audio_iface->setEnabled( true );
	ui->groupBox_redstart_network->setEnabled( true );
	ui->groupBox_session_list->setEnabled( true );
	ui->groupBox_session_details->setEnabled( true );
	ui->groupBox_core->setEnabled( false );
	ui->groupBox_paths->setEnabled( false );
	ui->groupBox_macros->setEnabled( false );
	ui->menuRun->setEnabled( false );
	ui->menuNew_session->setEnabled( true );
	ui->groupBox_core_control->setEnabled( false );
	ui->graphicsView_input_levels->setEnabled( false );
	ui->graphicsView_output_levels->setEnabled( false );
	ui->groupBox_global_am->setEnabled( false );
	ui->groupBox_clients->setEnabled( false );
	ui->pushButton_add_local_path->setEnabled( false );


	// Clear
	ui->lineEdit_core_settings_version->clear();
	ui->listView_paths->reset();

	// Reset elements
	bool bPreserveCoreControlValues = m_qSettings.value( "PreserveCoreControlValues", true ).toBool(); // @todo control this setting via preferences dialog
	if( !bPreserveCoreControlValues )
	{
		emit InputSignalChangedDecibel( 0.0f );
		emit OutputSignalChangedDecibel( 0.0f );
		emit InputIsMutedChanged( false );
		emit OutputIsMutedChanged( false );
	}
}

void RedstartWindow::RestoreWindowSize()
{
	restoreGeometry( m_qSettings.value( "Redstart/gui/WindowGeometry" ).toByteArray() );
	restoreState( m_qSettings.value( "Redstart/gui/WindowState" ).toByteArray() );
}

void RedstartWindow::HandleVAEvent( const CVAEvent* pEvent )
{
	// Handle pushable events
	switch( pEvent->iEventType )
	{
	case CVAEvent::PROGRESS_UPDATE:
		//ui->statusBar->showMessage( pEvent->sParam.c_str() );
		break;
	case CVAEvent::MEASURES_UPDATE:
		if( pEvent->sParam == "RenderingModule" )
			; // @todo update rendering module GUI for sObjectID
		else if( pEvent->sParam == "ReproductionModule" )
			; // @todo update reproduction module GUI for sObjectID
		else if( pEvent->sParam.empty() )
			UpdateMeasures( pEvent->vfInputRMSs, pEvent->vfInputPeaks, pEvent->vfOutputRMSs, pEvent->vfOutputPeaks ); // Core i/o
		break;
	}

	/* Don't handle events that have been triggered from this interface connection
	if( pEvent->pSender == m_pVAInterface )
	return;
	*/

	switch( pEvent->iEventType )
	{
	case CVAEvent::GLOBAL_AURALIZATION_MODE_CHANGED:
		emit GlobalAuralizationModeChanged( pEvent->iAuralizationMode );
		break;
	}

	switch( pEvent->iEventType )
	{
	case CVAEvent::INPUT_GAIN_CHANGED:
		emit InputSignalChangedDecibel( ratio_to_db20( pEvent->dVolume ) );
		break;
	case CVAEvent::OUTPUT_GAIN_CHANGED:
		emit OutputSignalChangedDecibel( ratio_to_db20( pEvent->dVolume ) );
		break;
	case CVAEvent::INPUT_MUTING_CHANGED:
		emit InputIsMutedChanged( pEvent->bMuted );
		break;
	case CVAEvent::OUTPUT_MUTING_CHANGED:
		emit OutputIsMutedChanged( pEvent->bMuted );
		break;
	}
}

void RedstartWindow::UpdateMeasures( const std::vector< float >& vfInputRMSs, const std::vector< float >& vfInputPeaks, const std::vector< float >& vfOutputRMSs, const std::vector< float >& vfOutputPeaks )
{
	int iNumInputChannels = ( int ) vfInputRMSs.size();
	assert( vfInputRMSs.size() == vfInputPeaks.size() );
	if( ui->graphicsView_input_levels->GetNumChannels() != iNumInputChannels )
		ui->graphicsView_input_levels->SetNumChannels( iNumInputChannels );

	ui->graphicsView_input_levels->SetRMSs( vfInputRMSs );
	ui->graphicsView_input_levels->SetPeaks( vfInputPeaks );

	int iNumOutputChannels = ( int ) vfOutputRMSs.size();
	assert( vfOutputRMSs.size() == vfOutputPeaks.size() );
	if( ui->graphicsView_output_levels->GetNumChannels() != iNumOutputChannels )
		ui->graphicsView_output_levels->SetNumChannels( iNumOutputChannels );

	ui->graphicsView_output_levels->SetRMSs( vfOutputRMSs );
	ui->graphicsView_output_levels->SetPeaks( vfOutputPeaks );
}

void RedstartWindow::AcquireAsioDevices()
{
	std::vector< CAudioDeviceSpecs > voAudioDevices = m_voAudioDevices;

	try
	{
		ITAsioInitializeLibrary();

		long lDrivers = ITAsioGetNumDrivers();
		if( lDrivers <= 0 )
			ITA_EXCEPT1( INVALID_PARAMETER, "No ASIO-compatibel drivers could be found on this system." );

		m_voAudioDevices.clear();

		for( long i = 0; i < lDrivers; i++ )
		{
			CAudioDeviceSpecs oDeviceSpecs;
			oDeviceSpecs.iDriverNumber = i;
			oDeviceSpecs.iBackend = AudioBackend::ASIO;
			oDeviceSpecs.sName = ITAsioGetDriverName( i );

			try
			{
				ITAsioInitializeDriver( i );

				long in = 0;
				long out = 0;
				ITAsioGetChannels( &in, &out );
				oDeviceSpecs.iNumInputChannels = int( in );
				oDeviceSpecs.iNumOutputChannels = int( out );

				ITAsioFinalizeDriver();

				oDeviceSpecs.bInitializable = true;

			}
			catch( const ITAException& )
			{
				oDeviceSpecs.bInitializable = false;
				continue;
			}

			if( oDeviceSpecs.bInitializable )
				m_voAudioDevices.push_back( oDeviceSpecs );
		}

		ITAsioFinalizeLibrary();
	}
	catch( const ITAException& e )
	{
		QString sErrorMessage = QString( "Error during ASIO device acquisition: %1" ).arg( QString::fromStdString( e.ToString() ) );
		QErrorMessage oErrMsg;
		oErrMsg.showMessage( sErrorMessage );
		oErrMsg.exec();
	}

	for( const CAudioDeviceSpecs oDevice : voAudioDevices )
		if( oDevice.iBackend != AudioBackend::ASIO )
			m_voAudioDevices.push_back( oDevice );
}

void RedstartWindow::PopulateAudioDevices()
{
	ui->comboBox_audio_iface_device->clear();
	int iCurrentBackend = ui->comboBox_audio_driver->currentIndex();
	for( const CAudioDeviceSpecs& oDevice : m_voAudioDevices )
	{
		if( oDevice.iBackend == iCurrentBackend )
			ui->comboBox_audio_iface_device->addItem( oDevice.sName, QVariant( oDevice.iDriverNumber ) );
	}

	if( ui->comboBox_audio_driver->currentIndex() == AudioBackend::PORTAUDIO )
	{
		ui->checkBox_portaudio_default_device->setEnabled( true );
		if( ui->checkBox_portaudio_default_device->isChecked() )
			ui->comboBox_audio_iface_device->setEnabled( false );
		else
			ui->comboBox_audio_iface_device->setEnabled( true );
	}
	else
	{
		ui->checkBox_portaudio_default_device->setEnabled( false );
		ui->comboBox_audio_iface_device->setEnabled( true );
	}
}

void RedstartWindow::AcquirePortaudioDevices()
{
	std::vector< CAudioDeviceSpecs > voAudioDevices = m_voAudioDevices;

	try
	{
		ITAPortaudioInterface oPortaudio( 44100, 1024 );
		oPortaudio.Initialize();
		int lDrivers = oPortaudio.GetNumDevices();

		if( lDrivers <= 0 )
			ITA_EXCEPT1( INVALID_PARAMETER, "No Portaudio output devices could be found on this system." );

		m_iPortaudioDefaultDevice = oPortaudio.GetDefaultOutputDevice();

		m_voAudioDevices.clear();

		for( long i = 0; i < lDrivers; i++ )
		{
			CAudioDeviceSpecs oDeviceSpecs;
			oDeviceSpecs.iDriverNumber = i;
			oDeviceSpecs.iBackend = AudioBackend::PORTAUDIO;
			oDeviceSpecs.sName = oPortaudio.GetDeviceName( i ).c_str();

			try
			{
				oPortaudio.GetNumChannels( i, oDeviceSpecs.iNumInputChannels, oDeviceSpecs.iNumOutputChannels );

				oDeviceSpecs.bInitializable = true; // only a guess

				if( i == oPortaudio.GetDefaultOutputDevice() )
					oDeviceSpecs.bDefaultDevice = true;
				else
					oDeviceSpecs.bDefaultDevice = false;

			}
			catch( const ITAException& )
			{
				oDeviceSpecs.bInitializable = false;
				continue;
			}

			if( oDeviceSpecs.bInitializable )
				m_voAudioDevices.push_back( oDeviceSpecs );
		}

		oPortaudio.Finalize();
	}
	catch( const ITAException& e )
	{
		QString sErrorMessage = QString( "Error during Portaudio device acquisition: %1" ).arg( QString::fromStdString( e.ToString() ) );
		QErrorMessage oErrMsg;
		oErrMsg.showMessage( sErrorMessage );
		oErrMsg.exec();
	}

	for( const CAudioDeviceSpecs oDevice : voAudioDevices )
		if( oDevice.iBackend != AudioBackend::ASIO )
			m_voAudioDevices.push_back( oDevice );
}

void RedstartWindow::on_pushButton_start_clicked()
{
	try
	{
		if( ui->checkBox_redstart_network_connect_as_client->isChecked() == false )
		{
			CVAStruct oVAConfigArgs = ui->listView_redstart_session_list->GetCurrentConfig();

			int iBlockLength = -1;
			int iOutputChannels = -1;

			// Override configs
			std::string sBackend, sDevice;
			switch( ui->comboBox_audio_driver->currentIndex() )
			{
			case AudioBackend::ASIO:
			{
				sBackend = "ASIO";
				sDevice = ui->comboBox_audio_iface_device->currentText().toStdString();
				break;
			}
			case AudioBackend::VIRTUAL:
			{
				sBackend = "Virtual";
				sDevice = "Trigger";
				iBlockLength = 128;
				iOutputChannels = 2;
				break;
			}
			case AudioBackend::PORTAUDIO:
			default:
			{
				sBackend = "Portaudio";
				if( ui->checkBox_portaudio_default_device->isChecked() )
					sDevice = "default";
				else
					sDevice = ui->comboBox_audio_iface_device->currentIndex();
			}
			}

			oVAConfigArgs[ "Audio driver" ] = CVAStruct();
			oVAConfigArgs[ "Audio driver" ][ "Driver" ] = sBackend;
			oVAConfigArgs[ "Audio driver" ][ "Device" ] = sDevice;
			if( iBlockLength > 0 )
				oVAConfigArgs[ "Audio driver" ][ "BufferSize" ] = iBlockLength;
			if( iOutputChannels > 0 )
				oVAConfigArgs[ "Audio driver" ][ "OutputChannels" ] = iOutputChannels;

			if( !oVAConfigArgs.HasKey( "Paths" ) )
				oVAConfigArgs[ "Paths" ] = CVAStruct();
			oVAConfigArgs[ "Paths" ][ "redstart_data" ] = "data";
			oVAConfigArgs[ "Paths" ][ "redstart_data_dev" ] = "../VACore/data";

			m_pVAInterface = VACore::CreateCoreInstance( oVAConfigArgs, ui->plainTextEdit_core_output->GetOutputStream() );

			m_pVAInterface->Initialize();
			m_pVAInterface->AttachEventHandler( this );
			m_pVANetServer->SetCoreInstance( m_pVAInterface );

			bool bOK;
			int iPort = ui->lineEdit_redstart_network_port->text().toInt( &bOK );
			int iStatus = -1;
			if( bOK )
				iStatus = m_pVANetServer->Initialize( ui->lineEdit_redstart_network_address->text().toStdString(), iPort );
			else
				iStatus = m_pVANetServer->Initialize( ui->lineEdit_redstart_network_address->text().toStdString() );

			if( m_pVAInterface->GetState() == IVAInterface::VA_CORESTATE_READY && iStatus == IVANetServer::VA_NO_ERROR )
			{
				ui->statusBar->showMessage( "VA core successfully initialized and network server started. Running session " + ui->listView_redstart_session_list->GetCurrentSessionID() + "." );

				ui->pushButton_add_local_path->setEnabled( true );

				PostCoreStart();
			}
			else
			{
				ui->statusBar->showMessage( "VA core initialization failed." );
				m_pVANetServer->Finalize();

				delete m_pVAInterface;
				m_pVAInterface = NULL;
			}

		}
		else
		{
			// Client mode
			if( m_pVANetClient->IsConnected() )
				m_pVANetClient->Disconnect();

			bool bOK;
			int iPort = ui->lineEdit_redstart_network_port->text().toInt( &bOK );
			int iStatus = -1;
			if( bOK )
				iStatus = m_pVANetClient->Initialize( ui->lineEdit_redstart_network_address->text().toStdString(), iPort );
			else
				iStatus = m_pVANetClient->Initialize( ui->lineEdit_redstart_network_address->text().toStdString() );

			if( iStatus != IVANetClient::VA_NO_ERROR )
			{
				VA_EXCEPT2( INVALID_PARAMETER, "Could not initialize client, connection failed." );
			}

			m_pVAInterface = m_pVANetClient->GetCoreInstance();

			ui->pushButton_add_local_path->setEnabled( false );

			// @todo attach network event handler to detect server connection loss or abortion
			//m_pVANetClient->AttachEventHandler( this );
		}
	}
	catch( const CVAException& ex )
	{

		if( m_pVAInterface )
		{
			delete m_pVAInterface;
			m_pVAInterface = NULL;
		}

		ui->statusBar->showMessage( QString::fromStdString( ex.ToString() ) + ". Will deactivate auto-start checkbox." );
		ui->checkBox_redstart_auto_start->setChecked( false );
	}
}

void RedstartWindow::on_pushButton_stop_clicked()
{
	try
	{
		if( ui->checkBox_redstart_network_connect_as_client->isChecked() == false )
		{
			if( m_pVAInterface )
			{
				m_pVANetServer->Finalize();

				if( m_pVAInterface->GetState() == IVAInterface::VA_CORESTATE_READY )
					m_pVAInterface->Finalize();

				delete m_pVAInterface;
				m_pVAInterface = nullptr;

				ui->statusBar->showMessage( "VA core has been stopped." );
				PostCoreStop();
			}
		}
		else
		{
			// Client mode
			if( m_pVANetClient->IsConnected() )
				m_pVANetClient->Disconnect();

			m_pVAInterface = nullptr;
		}
	}
	catch( const CVAException& ex )
	{

		if( m_pVAInterface &&  ui->checkBox_redstart_network_connect_as_client->isChecked() == false )
		{
			delete m_pVAInterface;
			m_pVAInterface = NULL;
		}

		ui->statusBar->showMessage( QString::fromStdString( ex.ToString() ) + ". Will deactivate auto-start checkbox." );
		ui->checkBox_redstart_auto_start->setChecked( false );
	}
}

void RedstartWindow::on_shortcut_start_stop()
{
	if( m_pVAInterface )
		on_pushButton_stop_clicked();
	else
		on_pushButton_start_clicked();
}

void RedstartWindow::on_pushButton_refresh_clicked()
{
	const int iAudioBackend = ui->comboBox_audio_driver->currentIndex();
	if( iAudioBackend == AudioBackend::PORTAUDIO )
	{
		AcquirePortaudioDevices();
	}
	else
	{
		AcquireAsioDevices();
	}

	PopulateAudioDevices();
}

void RedstartWindow::on_comboBox_audio_driver_currentIndexChanged( int )
{
	PopulateAudioDevices();
}

void RedstartWindow::on_comboBox_audio_iface_device_currentIndexChanged( int )
{

}

void RedstartWindow::on_actionWebsite_triggered()
{
	QUrl urlVAWebsite( "http://www.virtualacoustics.org" );
	QDesktopServices::openUrl( urlVAWebsite );
}

void RedstartWindow::on_actionGet_started_triggered()
{
	QUrl urlVAWebsiteGetStarted( "http://www.virtualacoustics.org/start.html" );
	QDesktopServices::openUrl( urlVAWebsiteGetStarted );
}

void RedstartWindow::on_actionDocumentation_triggered()
{
	QUrl urlVAWebsiteDocumentation( "http://www.virtualacoustics.org/documentation.html" );
	QDesktopServices::openUrl( urlVAWebsiteDocumentation );
}

void RedstartWindow::on_actionGet_help_triggered()
{
	QUrl urlVAWebsiteGetHelp( "http://www.virtualacoustics.org/help.html" );
	QDesktopServices::openUrl( urlVAWebsiteGetHelp );
}

void RedstartWindow::on_checkBox_redstart_network_connect_as_client_clicked()
{
	if( ui->checkBox_redstart_network_connect_as_client->isChecked() )
		ui->groupBox_redstart_audio_iface->setEnabled( false );
	else
		ui->groupBox_redstart_audio_iface->setEnabled( true );
}

void RedstartWindow::on_checkBox_portaudio_default_device_clicked()
{
	if( ui->checkBox_portaudio_default_device->isChecked() )
	{
		ui->comboBox_audio_iface_device->setEnabled( false );
		if( ui->comboBox_audio_iface_device->count() >= m_iPortaudioDefaultDevice )
			ui->comboBox_audio_iface_device->setCurrentIndex( m_iPortaudioDefaultDevice );
	}
	else
		ui->comboBox_audio_iface_device->setEnabled( true );
}

void RedstartWindow::on_listView_redstart_session_list_clicked( const QModelIndex &index )
{
	if( ui->listView_redstart_session_list->model()->data( index ).isValid() )
	{
		std::string sSessionName = ui->listView_redstart_session_list->model()->data( index ).toString().toStdString();
		CVAStruct oStruct = ui->listView_redstart_session_list->GetCurrentConfig();
		ui->treeView_session_details->SetStruct( oStruct );
	}
}

void RedstartWindow::on_actionBinauralHeadphones_triggered()
{
	RedstartSessionBinauralHeadphonesDialog d( this );
	if( d.exec() )
	{
		// Enforce unique session id if id is already existing
		QString sNewID = d.GetSessionName();
		while( ui->listView_redstart_session_list->SessionNameExists( sNewID ) )
			sNewID += "_new";
		ui->listView_redstart_session_list->AddSession( sNewID, d.GetCoreConfig() ); // In any case save the session so no data is lost

		CVAStruct oStruct = ui->listView_redstart_session_list->GetCurrentConfig();
		ui->treeView_session_details->SetStruct( oStruct );

		if( d.GetStartImmediately() )
			on_pushButton_start_clicked();
	}
}

void RedstartWindow::on_actionImport_session_triggered()
{
	RedstartSessionImportDialog d( this );
	if( d.exec() )
	{
		// Enforce unique session id if id is already existing
		QString sNewID = d.GetSessionName();
		while( ui->listView_redstart_session_list->SessionNameExists( sNewID ) )
			sNewID += "_new";
		ui->listView_redstart_session_list->AddSession( sNewID, d.GetCoreConfig() ); // In any case save the session so no data is lost
		CVAStruct oStruct = ui->listView_redstart_session_list->GetCurrentConfig();
		ui->treeView_session_details->SetStruct( oStruct );

		if( d.GetStartImmediately() )
			on_pushButton_start_clicked();
	}
}

void RedstartWindow::on_actionDefault_experimental_session_triggered()
{
	RedstartSessionExperimentalTalkthroughDialog d( this );
	if( d.exec() )
	{
		// Enforce unique session id if id is already existing
		QString sNewID = d.GetSessionName();
		while( ui->listView_redstart_session_list->SessionNameExists( sNewID ) )
			sNewID += "_new";
		ui->listView_redstart_session_list->AddSession( sNewID, d.GetCoreConfig() ); // In any case save the session so no data is lost

		CVAStruct oStruct = ui->listView_redstart_session_list->GetCurrentConfig();
		ui->treeView_session_details->SetStruct( oStruct );

		if( d.GetStartImmediately() )
			on_pushButton_start_clicked();
	}
}

void RedstartWindow::on_actionCirculating_source_triggered()
{
	if( !m_pVAInterface )
		return;

	m_pCirculatingSourceDialog->SetVAInterface( m_pVAInterface );
	m_pCirculatingSourceDialog->exec();
	m_pCirculatingSourceDialog->hide();
}

void RedstartWindow::on_actionAmbisonics_triggered()
{

}

void RedstartWindow::on_actionExport_to_file_triggered()
{
	QFileDialog fd;
	fd.setNameFilters( QStringList() << "INI files (*.ini)" << "Any file (*.*)" );
	fd.setViewMode( QFileDialog::Detail );
	fd.setAcceptMode( QFileDialog::AcceptSave );

	QDir oLastDir( m_qSettings.value( "Redstart/last_browse_folder" ).toString() );
	if( oLastDir.exists() )
		fd.setDirectory( oLastDir );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	QString sFileName = ui->listView_redstart_session_list->GetCurrentSessionID();
	if( sFileName.isEmpty() == false )
		fd.selectFile( sFileName + ".ini" );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			QFileInfo oFile( sFilePath );
			if( fd.directory().exists() )
				m_qSettings.setValue( "Redstart/last_browse_folder", oFile.absolutePath() );

			try
			{
				CVAStruct oConfig = ui->listView_redstart_session_list->GetCurrentConfig();
				VACore::StoreCoreConfigToFile( oConfig, sFilePath.toStdString() );
			}
			catch( CVAException& e )
			{
				QString sErrorMessage = QString( "Could not export session configuration: %1" ).arg( QString::fromStdString( e.ToString() ) );
				QErrorMessage oErrMsg;
				oErrMsg.showMessage( sErrorMessage );
				oErrMsg.exec();
			}
		}
	}
}

void RedstartWindow::on_actionRemove_triggered()
{
	ui->listView_redstart_session_list->RemoveCurrentSession();

	CVAStruct oStruct;
	if( !ui->listView_redstart_session_list->GetCurrentSessionID().isEmpty() )
		oStruct = ui->listView_redstart_session_list->GetCurrentConfig();

	ui->treeView_session_details->SetStruct( oStruct );
}
void RedstartWindow::on_actionAbout_Redstart_triggered()
{
	RedstartAboutDialog d( this );
	d.exec();
}

void RedstartWindow::on_actionEdit_session_triggered()
{
	RedstartSessionWizardDialog d( this );

	QString sCurrentSessionID = ui->listView_redstart_session_list->GetCurrentSessionID();
	CVAStruct oCurrentConfig;
	if( sCurrentSessionID.isEmpty() )
		return;

	oCurrentConfig = ui->listView_redstart_session_list->GetCurrentConfig();
	d.SetSession( sCurrentSessionID, ConvertVAStructToQHash( oCurrentConfig ) );

	if( d.exec() )
	{
		bool bEnforceDuplicate = true;
		if( sCurrentSessionID != d.GetSessionName() && !ui->listView_redstart_session_list->SessionNameExists( d.GetSessionName() ) )
		{
			ui->listView_redstart_session_list->RenameSession( sCurrentSessionID, d.GetSessionName() );
			bEnforceDuplicate = false;
		}

		QVariantHash oNewConfig = d.GetCoreConfig();
		if( d.GetDuplicationRequested() || bEnforceDuplicate )
		{
			// Enforce unique session id if duplicate requested or session should be renamed to an already existing session
			QString sNewID = d.GetSessionName();
			while( ui->listView_redstart_session_list->SessionNameExists( sNewID ) )
				sNewID += "_copy";

			ui->listView_redstart_session_list->AddSession( sNewID, oNewConfig );
		}
		else
		{
			ui->listView_redstart_session_list->UpdateSession( d.GetSessionName(), oNewConfig );
		}

		ui->treeView_session_details->SetStruct( ConvertQHashToVAStruct( oNewConfig ) );
	}
}

void RedstartWindow::on_actionSession_wizard_triggered()
{
	RedstartSessionWizardDialog d( this );

	d.SetDuplicationOptionDeactivated();

	if( d.exec() )
	{
		QVariantHash oNewConfig = d.GetCoreConfig();
		if( ui->listView_redstart_session_list->SessionNameExists( d.GetSessionName() ) )
		{
			// Enforce unique session id if id is already existing
			QString sNewID = d.GetSessionName();
			while( ui->listView_redstart_session_list->SessionNameExists( sNewID ) )
				sNewID += "_new";
			ui->listView_redstart_session_list->AddSession( sNewID, oNewConfig ); // In any case save the session so no data is lost
		}
		else
		{
			ui->listView_redstart_session_list->AddSession( d.GetSessionName(), oNewConfig );
		}

		ui->treeView_session_details->SetStruct( ConvertQHashToVAStruct( oNewConfig ) );
	}
}

void RedstartWindow::on_actionDuplicate_current_session_triggered()
{
	RedstartSessionWizardDialog d( this );

	QString sCurrentSessionID = ui->listView_redstart_session_list->GetCurrentSessionID();
	CVAStruct oCurrentConfig;
	if( sCurrentSessionID.isEmpty() )
		return;

	oCurrentConfig = ui->listView_redstart_session_list->GetCurrentConfig();
	d.SetSession( sCurrentSessionID, ConvertVAStructToQHash( oCurrentConfig ) );

	d.SetDuplicationOptionActivated();

	if( d.exec() )
	{
		QVariantHash oNewConfig = d.GetCoreConfig();
		if( ui->listView_redstart_session_list->SessionNameExists( d.GetSessionName() ) )
		{
			// Enforce unique session id if id is already existing
			QString sNewID = d.GetSessionName();
			while( ui->listView_redstart_session_list->SessionNameExists( sNewID ) )
				sNewID += "_copy";
			ui->listView_redstart_session_list->AddSession( sNewID, oNewConfig ); // In any case save the session so no data is lost
		}
		else
		{
			ui->listView_redstart_session_list->AddSession( d.GetSessionName(), oNewConfig );
		}

		ui->treeView_session_details->SetStruct( ConvertQHashToVAStruct( oNewConfig ) );
	}
}

void RedstartWindow::CoreChangeInputSignalDecibel( int iLevelDB )
{
	emit InputSignalChangedDecibel( iLevelDB );
	emit InputSignalChangedDecibel( ( double ) iLevelDB );
}

void RedstartWindow::CoreChangeInputSignalDecibel( double dLevelDB )
{
	emit InputSignalChangedDecibel( dLevelDB );
	emit InputSignalChangedDecibel( ( int ) dLevelDB );

	if( m_pVAInterface )
	{
		const double dGain = db20_to_ratio( dLevelDB );
		m_pVAInterface->SetInputGain( dGain );
	}
}

void RedstartWindow::CoreChangeInputIsMuted( int iMuted )
{
	emit InputIsMutedChanged( ( bool ) iMuted );

	if( m_pVAInterface )
		m_pVAInterface->SetInputMuted( ( bool ) iMuted );
}

void RedstartWindow::CoreChangeOutputSignalDecibel( int iLevelDB )
{
	emit OutputSignalChangedDecibel( iLevelDB );
	emit OutputSignalChangedDecibel( ( double ) iLevelDB );
}

void RedstartWindow::CoreChangeOutputSignalDecibel( double dLevelDB )
{
	emit OutputSignalChangedDecibel( dLevelDB );
	emit OutputSignalChangedDecibel( ( int ) dLevelDB );

	if( m_pVAInterface )
	{
		const double dGain = db20_to_ratio( dLevelDB );
		m_pVAInterface->SetOutputGain( dGain );
	}
}

void RedstartWindow::CoreChangeOutputIsMuted( int iMuted )
{
	emit OutputIsMutedChanged( ( bool ) iMuted );

	if( m_pVAInterface )
		m_pVAInterface->SetOutputMuted( ( bool ) iMuted );
}

void RedstartWindow::CoreChangeAuralizationMode( bool bActivateMode, int iMode )
{
	if( !m_pVAInterface )
		return;

	int iAM = m_pVAInterface->GetGlobalAuralizationMode();

	if( bActivateMode == true )
		iAM |= iMode;
	else
		iAM &= ~iMode;

	m_pVAInterface->SetGlobalAuralizationMode( iAM );
}

void RedstartWindow::CoreChangeAuralizationModeDS( int iState )
{
	emit AuralizationModeDSChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_DIRECT_SOUND );
}

void RedstartWindow::CoreChangeAuralizationModeAB( int iState )
{
	emit AuralizationModeABChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_ABSORPTION );
}

void RedstartWindow::CoreChangeAuralizationModeDF( int iState )
{
	emit AuralizationModeDFChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_DIFFRACTION );
}

void RedstartWindow::CoreChangeAuralizationModeDD( int iState )
{
	emit AuralizationModeDDChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_DIFFUSE_DECAY );
}

void RedstartWindow::CoreChangeAuralizationModeDP( int iState )
{
	emit AuralizationModeDPChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_DOPPLER );
}

void RedstartWindow::CoreChangeAuralizationModeER( int iState )
{
	emit AuralizationModeERChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_EARLY_REFLECTIONS );
}

void RedstartWindow::CoreChangeAuralizationModeMA( int iState )
{
	emit AuralizationModeMAChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_MEDIUM_ABSORPTION );
}

void RedstartWindow::CoreChangeAuralizationModeNF( int iState )
{
	emit AuralizationModeNFChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_NEARFIELD );
}

void RedstartWindow::CoreChangeAuralizationModeSC( int iState )
{
	emit AuralizationModeSCChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_SCATTERING );
}

void RedstartWindow::CoreChangeAuralizationModeDIR( int iState )
{
	emit AuralizationModeDIRChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_SOURCE_DIRECTIVITY );
}

void RedstartWindow::CoreChangeAuralizationModeSL( int iState )
{
	emit AuralizationModeSLChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_SPREADING_LOSS );
}

void RedstartWindow::CoreChangeAuralizationModeTV( int iState )
{
	emit AuralizationModeTVChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_TEMP_VAR );
}

void RedstartWindow::CoreChangeAuralizationModeTR( int iState )
{
	emit AuralizationModeTRChanged( ( bool ) iState );
	CoreChangeAuralizationMode( ( iState > 0 ), IVAInterface::VA_AURAMODE_TRANSMISSION );
}

void RedstartWindow::CoreChangeGlobalAuralizationMode( int iAM )
{
	// Only emit update when not matching UI state - in that case toggle
	if( ( ( iAM & IVAInterface::VA_AURAMODE_DIRECT_SOUND ) > 0 ) != ui->checkBox_am_ds->isChecked() )
		emit AuralizationModeDSChanged( !ui->checkBox_am_ds->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_ABSORPTION ) > 0 ) != ui->checkBox_am_ab->isChecked() )
		emit AuralizationModeABChanged( !ui->checkBox_am_ab->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_DIFFRACTION ) > 0 ) != ui->checkBox_am_dif->isChecked() )
		emit AuralizationModeDFChanged( !ui->checkBox_am_dif->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_DIFFUSE_DECAY ) > 0 ) != ui->checkBox_am_dd->isChecked() )
		emit AuralizationModeDDChanged( !ui->checkBox_am_dd->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_DOPPLER ) > 0 ) != ui->checkBox_am_dp->isChecked() )
		emit AuralizationModeDPChanged( !ui->checkBox_am_dp->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_EARLY_REFLECTIONS ) > 0 ) != ui->checkBox_am_er->isChecked() )
		emit AuralizationModeERChanged( !ui->checkBox_am_er->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_MEDIUM_ABSORPTION ) > 0 ) != ui->checkBox_am_ma->isChecked() )
		emit AuralizationModeMAChanged( !ui->checkBox_am_ma->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_NEARFIELD ) > 0 ) != ui->checkBox_am_nf->isChecked() )
		emit AuralizationModeNFChanged( !ui->checkBox_am_nf->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_SCATTERING ) > 0 ) && !ui->checkBox_am_sca->isChecked() )
		emit AuralizationModeSCChanged( !ui->checkBox_am_sca->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_SOURCE_DIRECTIVITY ) > 0 ) != ui->checkBox_am_dir->isChecked() )
		emit AuralizationModeDIRChanged( !ui->checkBox_am_dir->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_SPREADING_LOSS ) > 0 ) != ui->checkBox_am_sl->isChecked() )
		emit AuralizationModeSLChanged( !ui->checkBox_am_sl->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_TEMP_VAR ) > 0 ) != ui->checkBox_am_tv->isChecked() )
		emit AuralizationModeTVChanged( !ui->checkBox_am_tv->isChecked() );
	if( ( ( iAM & IVAInterface::VA_AURAMODE_TRANSMISSION ) > 0 ) != ui->checkBox_am_tr->isChecked() )
		emit AuralizationModeTRChanged( !ui->checkBox_am_tr->isChecked() );
}

void RedstartWindow::CoreChangeLogLevel( int iLogLevel )
{
	if( !m_pVAInterface )
		return;

	CVAStruct oArgs;
	oArgs[ "setloglevel" ] = iLogLevel;
	m_pVAInterface->CallModule( "VACore", oArgs );
}

void RedstartWindow::SporadicTimeout()
{
	if( m_pVANetServer->GetNumConnectedClients() != m_pNetServerClientsModel->rowCount() )
	{
		QStringList sHosts;
		for( int n = 0; n < m_pVANetServer->GetNumConnectedClients(); n++ )
			sHosts << m_pVANetServer->GetClientHostname( n ).c_str();
		m_pNetServerClientsModel->setStringList( sHosts );
	}

	if( m_pVAInterface )
	{
		ui->lineEdit_core_settings_core_clock->setText( QString::number( m_pVAInterface->GetCoreClock(), 'f', 1 ) );
	}
}

void RedstartWindow::on_actionRunSimpleExample_triggered()
{
	try
	{
		if( !m_pVAInterface )
			VA_EXCEPT2( INVALID_PARAMETER, "No VA interface available. Please start a session, first." );

		RunSimpleExample( m_pVAInterface );
	}
	catch( CVAException& e )
	{
		QString sErrorMessage = QString( "Could not run simple example scene: %1" ).arg( QString::fromStdString( e.ToString() ) );
		QErrorMessage oErrMsg;
		oErrMsg.showMessage( sErrorMessage );
		oErrMsg.exec();
	}
}
void RedstartWindow::on_pushButton_core_control_reset_clicked()
{
	if( m_pVAInterface )
		m_pVAInterface->Reset();
}

void RedstartWindow::on_actionReset_triggered()
{
	if( m_pVAInterface )
		m_pVAInterface->Reset();
}
