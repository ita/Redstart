/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_RUN_CIRCULATING_SOURCE_DIALOG
#define IW_REDSTART_RUN_CIRCULATING_SOURCE_DIALOG

#include <QDialog>
#include <QDir>
#include <QString>
#include <QSettings>
#include <QTimer>

class IVAInterface;

namespace Ui
{
	class RedstartRunCirculatingSourceDialog;
}

class RedstartRunCirculatingSourceDialog : public QDialog
{
	Q_OBJECT

public:
	explicit RedstartRunCirculatingSourceDialog( QWidget *parent );
	~RedstartRunCirculatingSourceDialog();

	void SetVAInterface( IVAInterface* );

	void Start();
	void Stop();

private slots:
	void on_pushButton_BrowseDemoSound_clicked();
	void on_pushButton_BrowseHRIR_clicked();
	void on_pushButton_run_clicked();
	void on_pushButton_start_clicked();
	void on_pushButton_stop_clicked();
	
	void UpdateScene();

private:
	Ui::RedstartRunCirculatingSourceDialog* ui;
	QDir m_oLastBasePath;
	QDir m_oHRIRBasePath;
	QDir m_oDemoSoundBasePath;
	QString m_sFileBaseName;
	QSettings m_qSettings;
	IVAInterface* m_pVAInterface;
	QTimer* m_qVAUpdateTimer;

	int m_iSourceID, m_iReceiverID, m_iHRIRID;
	QString m_sSignalID;

	double m_dAzimuthDEG, m_dElevationDEG;
	double m_dRuntimeSeconds;
	
	void StartUpdateTimer();
	void StopUpdateTimer();

	void CreateScene();
	void DeleteScene();

	void SetUIElementsEnabledWhenNotRunning( bool bNo );
};

#endif // IW_REDSTART_RUN_CIRCULATING_SOURCE_DIALOG
