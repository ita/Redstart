/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_WINDOW
#define IW_REDSTART_WINDOW

#include <QMainWindow>
#include <QSettings>
#include <QStringList>
#include <QTimer>

#include "RedstartSessionList.h"

#include <VAEvent.h>

#include <ITAPortaudioInterface.h>
#ifdef WIN32
#include <ITAAsioInterface.h>
#else
#include <ITAJACKInterface.h>
#endif


class IVAInterface;
class IVANetClient;
class IVANetServer;
class RedstartRunCirculatingSourceDialog;

namespace Ui
{
	class RedstartWindow;
}

class RedstartWindow : public QMainWindow, public IVAEventHandler
{
	Q_OBJECT

public:
	explicit RedstartWindow( bool bFailSafeMode = false, bool bAutoStart = false, bool bSkipConfig = false, QWidget* pParent = 0 );
	~RedstartWindow();

	enum AudioBackend
	{
		ASIO = 0,
		PORTAUDIO = 1,
		VIRTUAL = 2,
	};

	enum AudioSamplingRate
	{
		FS_44kHz = 0,
		FS_48kHz = 1,
		FS_96kHz = 2,
	};

	enum AudioBufferSize
	{
		AUTO = 0
	};

	struct CAudioDeviceSpecs
	{
		int iDriverNumber;
		QString sName;
		int iNumInputChannels;
		int iNumOutputChannels;
		bool bInitializable;
		bool bDefaultDevice;
		int iBackend;

		inline CAudioDeviceSpecs()
		{
			iDriverNumber = -1;
			iNumInputChannels = -1;
			iNumOutputChannels = -1;
			bInitializable = false;
			bDefaultDevice = false;
			iBackend = -1;
		};

		inline CAudioDeviceSpecs( const QVariantHash& oHash )
			: CAudioDeviceSpecs()
		{
			iDriverNumber = oHash[ "DriverNumber" ].toInt();
			sName = oHash[ "Name" ].toString();
			iNumInputChannels = oHash[ "NumInputChannels" ].toInt();
			iNumOutputChannels = oHash[ "NumOutputChannels" ].toInt();
			bInitializable = oHash[ "Initializable" ].toBool();
			bDefaultDevice = oHash[ "DefaultDevice" ].toBool();
			iBackend = oHash[ "Backend" ].toInt();
		};

		inline QVariantHash asHash() const
		{
			QVariantHash oHash;
			oHash[ "DriverNumber" ] = iDriverNumber;
			oHash[ "Name" ] = sName;
			oHash[ "NumInputChannels" ] = iNumInputChannels;
			oHash[ "NumOutputChannels" ] = iNumOutputChannels;
			oHash[ "Initializable" ] = bInitializable;
			oHash[ "DefaultDevice" ] = bDefaultDevice;
			oHash[ "Backend" ] = iBackend;
			return oHash;
		};

	};

	private slots:
	void on_actionQuit_triggered();

	void on_pushButton_start_clicked();
	void on_pushButton_stop_clicked();
	void on_shortcut_start_stop();

	void on_pushButton_refresh_clicked();

	void on_comboBox_audio_driver_currentIndexChanged( int index );

	void on_comboBox_audio_iface_device_currentIndexChanged( int index );

	void on_actionWebsite_triggered();

	void on_actionGet_started_triggered();

	void on_actionDocumentation_triggered();

	void on_actionGet_help_triggered();

	void on_checkBox_redstart_network_connect_as_client_clicked();
	void on_checkBox_portaudio_default_device_clicked();
	void on_listView_redstart_session_list_clicked( const QModelIndex &index );

	void on_actionBinauralHeadphones_triggered();
	void on_actionImport_session_triggered();

	void on_actionDefault_experimental_session_triggered();

	void on_actionRunSimpleExample_triggered();
	void on_actionCirculating_source_triggered();

	void on_pushButton_core_control_reset_clicked();

	void on_actionAmbisonics_triggered();

	void on_actionExport_to_file_triggered();
	void on_actionRemove_triggered();
	void on_actionEdit_session_triggered();
	void on_actionSession_wizard_triggered();
	void on_actionDuplicate_current_session_triggered();
	void on_actionAbout_Redstart_triggered();

	void CoreChangeInputSignalDecibel( int );
	void CoreChangeInputSignalDecibel( double );
	void CoreChangeInputIsMuted( int );

	void CoreChangeOutputSignalDecibel( int );
	void CoreChangeOutputSignalDecibel( double );
	void CoreChangeOutputIsMuted( int );

	void CoreChangeAuralizationModeDS( int );
	void CoreChangeAuralizationModeER( int );
	void CoreChangeAuralizationModeDD( int );
	void CoreChangeAuralizationModeDIR( int );
	void CoreChangeAuralizationModeDP( int );
	void CoreChangeAuralizationModeSL( int );
	void CoreChangeAuralizationModeDF( int );
	void CoreChangeAuralizationModeSC( int );;
	void CoreChangeAuralizationModeNF( int );
	void CoreChangeAuralizationModeTV( int );
	void CoreChangeAuralizationModeMA( int );
	void CoreChangeAuralizationModeAB( int );
	void CoreChangeAuralizationModeTR( int );
	void CoreChangeGlobalAuralizationMode( int );

	void CoreChangeLogLevel( int );

	void SporadicTimeout();

    void on_actionReset_triggered();

signals:
	void InputSignalChangedDecibel( int );
	void InputSignalChangedDecibel( double );
	void OutputSignalChangedDecibel( int );
	void OutputSignalChangedDecibel( double );
	void InputIsMutedChanged( bool );
	void OutputIsMutedChanged( bool );

	void AuralizationModeDSChanged( bool );
	void AuralizationModeERChanged( bool );
	void AuralizationModeDDChanged( bool );
	void AuralizationModeDIRChanged( bool );
	void AuralizationModeDPChanged( bool );
	void AuralizationModeSLChanged( bool );
	void AuralizationModeDFChanged( bool );
	void AuralizationModeSCChanged( bool );
	void AuralizationModeNFChanged( bool );
	void AuralizationModeTVChanged( bool );
	void AuralizationModeMAChanged( bool );
	void AuralizationModeABChanged( bool );
	void AuralizationModeTRChanged( bool );
	void GlobalAuralizationModeChanged( int );

private:

	Ui::RedstartWindow* ui;
	IVAInterface* m_pVAInterface;
	IVANetClient* m_pVANetClient;
	IVANetServer* m_pVANetServer;

	std::vector< CAudioDeviceSpecs > m_voAudioDevices;
	int m_iPortaudioDefaultDevice;

	QStringListModel* m_pPathsModel;
	QStringListModel* m_pMacrosModel;
	QStringListModel* m_pNetServerClientsModel;

	QSettings m_qSettings;
	QTimer m_qSporadicTimer;

	RedstartSessionListView* pSessionListModel;

	RedstartRunCirculatingSourceDialog* m_pCirculatingSourceDialog;

	void LoadConfiguration();
	void StoreConfiguration();

	void RestoreWindowSize();
	void ConnectSignalsAndSlots();

	void PopulateAudioDevices();
	void AcquireAsioDevices();
	void AcquirePortaudioDevices();

	void PostCoreStart();
	void PostCoreStop();

	void HandleVAEvent( const CVAEvent* );
	void UpdateMeasures( const std::vector< float >& vfInputRMSs, const std::vector< float >& vfInputPeaks, const std::vector< float >& vfOutputRMSs, const std::vector< float >& vfOutputPeaks );

	void CoreChangeAuralizationMode( bool, int );

};

#endif // IW_REDSTART_WINDOW
