/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "RedstartSessionExportDialog.h"
#include <ui_RedstartSessionExportDialog.h>
#include "RedstartUtils.h"

#include <VAException.h>
#include <VAStruct.h>
#include <VACore.h>

#include <QFileDialog>
#include <QSettings>

RedstartSessionExportDialog::RedstartSessionExportDialog( QWidget *parent )
	: QDialog( parent ),
	ui( new Ui::RedstartSessionExportDialog )
	, m_bStartImmediately( false )
{
	ui->setupUi( this );

	QDialog::setWindowTitle( "Export session" );

	QSettings qSettings;
	QString sLastBrowseFolder = qSettings.value( "Redstart/last_browse_folder" ).toString();
	std::string s = sLastBrowseFolder.toStdString();

	if( QDir( sLastBrowseFolder ).exists() )
		m_oLastBasePath.setCurrent( sLastBrowseFolder );
}

RedstartSessionExportDialog::~RedstartSessionExportDialog()
{
	if( m_oLastBasePath.exists() )
		m_qSettings.setValue( "Redstart/last_browse_folder", m_oLastBasePath.absolutePath() );
	delete ui;
}

QString RedstartSessionExportDialog::GetSessionName() const
{
	return ui->lineEdit_Export_config_file->text();
}

bool RedstartSessionExportDialog::ValidFile() const
{
	return m_oConfigFileBasePath.exists() && !m_oConfigFileBasePath.isDir();
}

bool RedstartSessionExportDialog::GetStartImmediately() const
{
	return m_bStartImmediately;
}

QVariantHash RedstartSessionExportDialog::GetCoreConfig() const
{
	if( !ValidFile() )
		VA_EXCEPT2( INVALID_PARAMETER, "Can not import from a directory or non-existing file" );
	CVAStruct oCoreConfig = VACore::LoadCoreConfigFromFile( m_oConfigFileBasePath.absoluteFilePath().toStdString() );
	return ConvertVAStructToQHash( oCoreConfig );
}

void RedstartSessionExportDialog::on_pushButton_CreateSession_clicked()
{
	accept();
	close();
}

void RedstartSessionExportDialog::on_pushButton_CreateAndStartSession_clicked()
{
	m_bStartImmediately = true;
	accept();
	close();
}

void RedstartSessionExportDialog::on_pushButton_select_import_file_clicked()
{
	QFileDialog fd;
	fd.setNameFilters( QStringList() << "INI files (*.ini)" << "Any file (*.*)" );
	fd.setViewMode( QFileDialog::Detail );

	if( m_oLastBasePath.exists() )
		fd.setDirectory( m_oLastBasePath );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			m_oConfigFileBasePath.setFile( sFilePath );
			if( m_oConfigFileBasePath.exists() )
				m_oLastBasePath = fd.directory();

			ui->lineEdit_Export_config_file->setText( m_oConfigFileBasePath.fileName() );
		}
	}
}
