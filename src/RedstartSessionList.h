/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_SESSION_LIST
#define IW_REDSTART_SESSION_LIST

#include <QListView>
#include <QStringListModel>
#include <QSettings>
#include <qnamespace.h>

#include <VAException.h>
#include <VAStruct.h>

#include "RedstartUtils.h"

class RedstartSessionListModel : public QStringListModel
{};


class RedstartSessionListView : public QListView
{
	Q_OBJECT
public:
	inline RedstartSessionListView( QWidget* pParent )
		: QListView( pParent )
		, m_pModel( NULL )
	{
		m_pModel = new RedstartSessionListModel();
		this->setModel( m_pModel );

		// Initilizes session container in settings, if not present
		if( !m_qSettings.contains( "Redstart/Sessions" ) )
			m_qSettings.setValue( "Redstart/Sessions", QMap< QString, QVariant>() );

		UpdateSessionList();

		QString sLastSessionName = m_qSettings.value( "Redstart/last_session" ).toString();
		SetCurrentSession( sLastSessionName );

		setEditTriggers( QAbstractItemView::NoEditTriggers );
		setSelectionMode( QAbstractItemView::SingleSelection );
	};

	inline ~RedstartSessionListView()
	{
		QString sCurrentSessionName = m_pModel->data( currentIndex(), 0 ).toString();
		m_qSettings.setValue( "Redstart/last_session", sCurrentSessionName );
	};

	inline void SetCurrentSession( const QString& sID )
	{
		QStringList list;
		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();
		QMap< QString, QVariant>::const_iterator cit = sessions.constBegin();
		int i = -1;
		while( cit != sessions.constEnd() )
		{
			i++;
			if( cit++.key() == sID && i < m_pModel->rowCount() )
			{
				setCurrentIndex( m_pModel->index( i, 0 ) );
				return;
			}
		}

		if( m_pModel->rowCount() > 0 )
			this->setCurrentIndex( m_pModel->index( 0, 0 ) );
	};

	inline void UpdateSessionList()
	{
		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();

		QStringList list;
		if( sessions.count() > 0 )
		{

			QMap< QString, QVariant>::const_iterator cit = sessions.constBegin();
			while( cit != sessions.constEnd() )
				list.push_back( cit++.key() );
		}
		m_pModel->setStringList( list );
	};

	inline QString GetCurrentSessionID() const
	{
		QModelIndex index = currentIndex();
		if( m_pModel->rowCount() == 0 || !index.isValid() )
			return QString();

		QString sSessionName = m_pModel->data( index, 0 ).toString();
		return sSessionName;
	};

	inline void RemoveCurrentSession()
	{
		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();
		sessions.remove( GetCurrentSessionID() );
		m_qSettings.setValue( "Redstart/Sessions", sessions );
		m_qSettings.sync();

		UpdateSessionList();

		if( m_pModel->rowCount() > 0 )
			setCurrentIndex( m_pModel->index( 0, 0 ) );
	};

	inline bool SessionNameExists( const QString& sID )
	{
		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();
		return ( sessions.find( sID ) != sessions.end() );
	};

	inline void AddSession( const QString& sID, const QVariantHash& oCoreConfig, const bool bMakeCurrent = true )
	{
		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();
		sessions[ sID ] = oCoreConfig; // More secure than insert, will also overwrite
		m_qSettings.setValue( "Redstart/Sessions", sessions );

		UpdateSessionList();

		if( bMakeCurrent )
			SetCurrentSession( sID );
	};

	inline void RenameSession( const QString& sCurrentID, const QString& sNewID )
	{
		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();
		sessions[ sNewID ] = sessions[ sCurrentID ];
		sessions.remove( sCurrentID );
		m_qSettings.setValue( "Redstart/Sessions", sessions );

		UpdateSessionList();
	};

	inline void UpdateSession( const QString& sID, const QVariantHash& oCoreConfig, const bool bMakeCurrent = true )
	{
		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();

		if( sessions.find( sID ) == sessions.end() )
			return;

		sessions[ sID ] = oCoreConfig;
		m_qSettings.setValue( "Redstart/Sessions", sessions );

		UpdateSessionList();

		if( bMakeCurrent )
			SetCurrentSession( sID );
	};

	inline CVAStruct GetCurrentConfig()
	{
		if( m_pModel->rowCount() == 0 )
			VA_EXCEPT2( INVALID_PARAMETER, "No session vailable, please create one first." );

		QMap< QString, QVariant > sessions = m_qSettings.value( "Redstart/Sessions" ).toMap();
		QString sSessionName = GetCurrentSessionID();

		CVAStruct oCoreConfig;
		if( sessions.contains( sSessionName ) )
			oCoreConfig = ConvertQHashToVAStruct( sessions[ sSessionName ].toHash() );
		else
			VA_EXCEPT2( INVALID_PARAMETER, "Could not find Redstart session with identifier '" + sSessionName.toStdString() + "' in session list" );

		if( !oCoreConfig.HasKey( "Macros" ) )
			oCoreConfig[ "Macros" ] = CVAStruct();

		CVAStruct& oMacros( oCoreConfig[ "Macros" ] );
		if( !oMacros.HasKey( "ProjectName" ) )
			oMacros[ "ProjectName" ] = "Redstart";

		return oCoreConfig;
	};

	public slots :

private:
	RedstartSessionListModel* m_pModel;
	QSettings m_qSettings;
};


#endif // IW_REDSTART_SESSION_LIST
