/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "RedstartSessionBinauralHeadphonesDialog.h"
#include <ui_RedstartSessionBinauralHeadphonesDialog.h>

#include <QFileDialog>
#include <QSettings>

#include "RedstartDefaultCoreConfig.h"

RedstartSessionBinauralHeadphonesDialog::RedstartSessionBinauralHeadphonesDialog( QWidget *parent )
	: QDialog( parent )
	, ui( new Ui::RedstartDialogSessionBinauralHeadphones )
	, m_bStartImmediately( false )
{
	ui->setupUi( this );

	QDialog::setWindowTitle( "Binaural headphones session" );

	QSettings qSettings;
	QString sLastBrowseFolder = qSettings.value( "Redstart/last_browse_folder" ).toString();
	std::string s = sLastBrowseFolder.toStdString();

	if( QDir( sLastBrowseFolder ).exists() )
		m_oLastBasePath.setCurrent( sLastBrowseFolder );

	if( ui->checkBox_hpir_auto_filter_length->isChecked() )
		ui->spinBox_inverse_hpir_filter_length->setEnabled( false );
}

RedstartSessionBinauralHeadphonesDialog::~RedstartSessionBinauralHeadphonesDialog()
{
	if( m_oLastBasePath.exists() )
		m_qSettings.setValue( "Redstart/last_browse_folder", m_oLastBasePath.absolutePath() );
	delete ui;
}

QVariantHash RedstartSessionBinauralHeadphonesDialog::GetCoreConfig() const
{
	QVariantHash oFinalCoreConfig;

	QVariantHash oPaths;
	if( m_oDemoSoundBasePath.exists() )
		oPaths[ "DemoSoundBasePath" ] = m_oDemoSoundBasePath.absolutePath();
	if( m_oHRIRBasePath.exists() && m_oHRIRBasePath != m_oDemoSoundBasePath )
		oPaths[ "DefaultHRIRBasePath" ] = m_oHRIRBasePath.absolutePath();
	if( m_oInverseHPIRBasePath.exists() && m_oInverseHPIRBasePath != m_oHRIRBasePath )
		oPaths[ "HPIRBaseFolder" ] = m_oInverseHPIRBasePath.absolutePath();

	oFinalCoreConfig[ "Paths" ] = oPaths;


	QString sHRIRMacro = ui->lineEdit_macro_DefaultHRIR->text();
	if( ui->checkBox_folders_as_search_path->isChecked() )
	{
		QFileInfo oHRIR( sHRIRMacro );
		sHRIRMacro = oHRIR.fileName();
	}

	QString sDemoSoundMacro = ui->lineEdit_macro_DemoSound->text();
	if( ui->checkBox_folders_as_search_path->isChecked() )
	{
		QFileInfo oDemoSound( sDemoSoundMacro );
		sDemoSoundMacro = oDemoSound.fileName();
	}

	QVariantHash oMacros;
	oMacros[ "DefaultHRIR" ] = sHRIRMacro;
	oMacros[ "DemoSound" ] = sDemoSoundMacro;

	oFinalCoreConfig[ "Macros" ] = oMacros;


	QVariantHash oDevice;
	oDevice[ "Type" ] = "HP";
	oDevice[ "Channels" ] = ui->lineEdit_headphone_channels->text();

	oFinalCoreConfig[ "OutputDevice:MyHP" ] = oDevice;

	QVariantHash oOutput;
	oOutput[ "Devices" ] = "MyHP";

	oFinalCoreConfig[ "Output:MyDesktopHP" ] = oOutput;


	if( ui->groupBox_headphone_equalization->isChecked() )
	{
		QVariantHash oReproduction;
		oReproduction[ "Class" ] = "Headphones";
		oReproduction[ "Outputs" ] = "MyDesktopHP";

		if( ui->checkBox_inverse_hpir_calibration_gain_db->isChecked() )
			oReproduction[ "HpIRInvCalibrationGainDecibel" ] = ui->doubleSpinBox_InvHPIRCalibrationGain->value();
		else
			oReproduction[ "HpIRInvCalibrationGain" ] = ui->doubleSpinBox_InvHPIRCalibrationGain->value(); // no decibel

		QString sHPIRFile = ui->lineEdit_inverse_hpir_file->text();
		if( ui->checkBox_folders_as_search_path->isChecked() )
		{
			QFileInfo oHRIR( sHPIRFile );
			sHPIRFile = oHRIR.fileName();
		}
		oReproduction[ "HpIRInvFile" ] = sHPIRFile;

		if( !ui->checkBox_hpir_auto_filter_length->isChecked() )
			oReproduction[ "HpIRInvFilterLength" ] = ui->spinBox_inverse_hpir_filter_length->value();

		oFinalCoreConfig[ "Reproduction:MyEqualizedHeadphones" ] = oReproduction;

		QVariantHash oRenderer;
		oRenderer[ "Class" ] = "BinauralFreeField";
		oRenderer[ "Reproductions" ] = "MyEqualizedHeadphones";

		oFinalCoreConfig[ "Renderer:MyBinauralRenderer" ] = oRenderer;
	}
	else
	{
		QVariantHash oReproduction;
		oReproduction[ "Class" ] = "Talkthrough";
		oReproduction[ "Outputs" ] = "MyDesktopHP";

		oFinalCoreConfig[ "Reproduction:MyTalkthroughHeadphones" ] = oReproduction;

		QVariantHash oRenderer;
		oRenderer[ "Class" ] = "BinauralFreeField";
		oRenderer[ "Reproductions" ] = "MyTalkthroughHeadphones";

		oFinalCoreConfig[ "Renderer:MyBinauralRenderer" ] = oRenderer;
	}

	return oFinalCoreConfig;
}

QString RedstartSessionBinauralHeadphonesDialog::GetSessionName() const
{
	return ui->lineEdit_session_name->text();
}

bool RedstartSessionBinauralHeadphonesDialog::GetStartImmediately() const
{
	return m_bStartImmediately;
}

void RedstartSessionBinauralHeadphonesDialog::on_pushButton_CreateSession_clicked()
{
	accept();
	close();
}

void RedstartSessionBinauralHeadphonesDialog::on_pushButton_CreateAndStartSession_clicked()
{
	m_bStartImmediately = true;
	accept();
	close();
}

void RedstartSessionBinauralHeadphonesDialog::on_pushButton_BrowseDemoSound_clicked()
{
	QFileDialog fd;
	fd.setNameFilter( "WAV files (*.wav)" );
	fd.setViewMode( QFileDialog::Detail );

	if( m_oLastBasePath.exists() )
		fd.setDirectory( m_oLastBasePath );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			QFile oFile( sFilePath );
			if( oFile.exists() )
			{
				m_oDemoSoundBasePath = fd.directory();
				m_oLastBasePath = m_oDemoSoundBasePath;
			}
			ui->lineEdit_macro_DemoSound->setText( oFile.fileName() );
		}
	}
}

void RedstartSessionBinauralHeadphonesDialog::on_pushButton_BrowseHRIR_clicked()
{
	QFileDialog fd;
	fd.setNameFilter( "DAFF files (*.daff)" );
	fd.setViewMode( QFileDialog::Detail );

	if( m_oLastBasePath.exists() )
		fd.setDirectory( m_oLastBasePath );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			QFile oFile( sFilePath );
			if( oFile.exists() )
			{
				m_oHRIRBasePath = fd.directory();
				m_oLastBasePath = m_oHRIRBasePath;

			}

			ui->lineEdit_macro_DefaultHRIR->setText( sFilePath );
		}
	}
}

void RedstartSessionBinauralHeadphonesDialog::on_pushButton_inverse_hpir_file_select_clicked()
{
	QFileDialog fd;
	fd.setNameFilter( "WAV files (*.wav)" );
	fd.setViewMode( QFileDialog::Detail );

	if( m_oLastBasePath.exists() )
		fd.setDirectory( m_oLastBasePath );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			QFile oFile( sFilePath );
			if( oFile.exists() )
			{
				m_oHRIRBasePath = fd.directory();
				m_oLastBasePath = m_oHRIRBasePath;
			}
			ui->lineEdit_inverse_hpir_file->setText( oFile.fileName() );
		}
	}
}

void RedstartSessionBinauralHeadphonesDialog::on_checkBox_hpir_auto_filter_length_clicked( bool bEnabled )
{
	ui->spinBox_inverse_hpir_filter_length->setEnabled( !bEnabled );
}
