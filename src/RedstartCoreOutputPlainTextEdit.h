/*
*  --------------------------------------------------------------------------------------------
*
*    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
*     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
*      VVV    VVV   AAA
*       VVV  VVV     AAA        Copyright 2015-2021
*        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
*         VVVV         AAA      RWTH Aachen University
*
*  --------------------------------------------------------------------------------------------
*/
#ifndef IW_REDSTART_CORE_OUTPUT_PLAIN_TEXT_EDIT
#define IW_REDSTART_CORE_OUTPUT_PLAIN_TEXT_EDIT

#include <QPlainTextEdit>
#include <QStringListModel>
#include <QMutex>

#include <iostream>


class RedstartCoreOutputPlainTextEdit : public QPlainTextEdit
{
	class CStreamBuffer : public std::streambuf
	{
	public:
		CStreamBuffer( RedstartCoreOutputPlainTextEdit* pParent )
			: m_pParent( pParent )
		{
			setp( 0, 0 );
			setg( 0, 0, 0 );
		};

		virtual int_type overflow( int_type c )
		{
			if( c != EOF && c != '\n' )
				m_pParent->write( ( char ) c );
			else
				m_pParent->append();

			return c;
		};

	private:
		RedstartCoreOutputPlainTextEdit* m_pParent;
	};

	class COutStream : public std::ostream
	{
	public:
		inline COutStream( RedstartCoreOutputPlainTextEdit* pParent )
			: sb( pParent )
			, std::ostream( &sb )
			, std::ios( 0 )
		{
		};

		inline ~COutStream() 
		{
			sb.pubsync();
		};

	private:
		CStreamBuffer sb;
	};

	Q_OBJECT
public:
	inline RedstartCoreOutputPlainTextEdit( QWidget* pParent )
		: QPlainTextEdit( pParent )
	{
		m_pOutStream = new COutStream( this );		
	};

	inline ~RedstartCoreOutputPlainTextEdit()
	{
		delete m_pOutStream;
		m_pOutStream = NULL;
	};

	inline std::ostream* GetOutputStream()
	{
		return m_pOutStream;
	};

	inline void append()
	{
		// Append a copy
		m_qLock.lock();
		appendPlainText( m_sLine );
		m_sLine.clear();
		m_qLock.unlock();
	};

	inline void write( char c )
	{
		m_qLock.lock();
		m_sLine += QString( 1, c );
		m_qLock.unlock();
	};

	inline void write( const std::string& str )
	{
		m_qLock.lock();
		m_sLine += QString( str.c_str() );
		m_qLock.unlock();
	};

	public slots :

private:
	COutStream* m_pOutStream;
	QString m_sLine;
	QMutex m_qLock;
};


#endif // IW_REDSTART_CORE_OUTPUT_PLAIN_TEXT_EDIT
