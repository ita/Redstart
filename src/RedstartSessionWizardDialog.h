/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_SESSION_WIZARD_DIALOG
#define IW_REDSTART_SESSION_WIZARD_DIALOG

#include <QDialog>
#include <QDir>
#include <QFileInfo>
#include <QString>
#include <QSettings>
#include <QVariantHash>

namespace Ui
{
	class RedstartSessionWizardDialog;
}

class RedstartSessionWizardDialog : public QDialog
{
	Q_OBJECT

public:
	explicit RedstartSessionWizardDialog( QWidget *parent );
	~RedstartSessionWizardDialog();
	QVariantHash GetCoreConfig() const;
	QString GetSessionName() const;
	void SetSession( const QString& sSessionName, const QVariantHash& oConfig );
	bool GetDuplicationRequested() const;
	void SetDuplicationOptionDeactivated();
	void SetDuplicationOptionActivated();

private slots:
	void on_pushButton_Save_clicked();

private:
	Ui::RedstartSessionWizardDialog* ui;
	QDir m_oLastBasePath;
	QSettings m_qSettings;
	QVariantHash m_oCurrentConfig;
};

#endif // IW_REDSTART_SESSION_WIZARD_DIALOG
