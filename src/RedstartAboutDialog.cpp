/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "RedstartAboutDialog.h"
#include <ui_RedstartAboutDialog.h>

RedstartAboutDialog::RedstartAboutDialog( QWidget *parent )
	: QDialog( parent )
	, ui( new Ui::RedstartAboutDialog )
{
	ui->setupUi( this );

	QDialog::setWindowTitle( "About Redstart (and VA)" );
}

RedstartAboutDialog::~RedstartAboutDialog()
{
	delete ui;
}
