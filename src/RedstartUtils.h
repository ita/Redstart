/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_UTILS
#define IW_REDSTART_UTILS

#include <QHash>
#include <QStandardItem>
#include <VAStruct.h>

#include <cassert>

//! Builds a standard item from a VA struct
/**
  * Creates items dynamically.
  */
inline void BuildStandardItemFromVAStruct( const CVAStruct& oStruct, QStandardItem* pRootNode )
{
	CVAStruct::const_iterator cit = oStruct.Begin();	
	while( cit != oStruct.End() )
	{
		const QString sKey( cit->first.c_str() );
		const CVAStructValue& oValue( cit->second );
		cit++;

		QList< QStandardItem* > pItemList;
		pItemList << new QStandardItem( sKey );		

		switch( oValue.GetDatatype() )
		{
		case CVAStructValue::BOOL:
		{
			pItemList << new QStandardItem( QVariant( bool( oValue ) ).toString() );
			pItemList << new QStandardItem( "Bool" );
			break;
		}
		case CVAStructValue::INT:
		{
			pItemList << new QStandardItem( QVariant( int( oValue ) ).toString() );
			pItemList << new QStandardItem( "Int" );
			break;
		}
		case CVAStructValue::DOUBLE:
		{
			pItemList << new QStandardItem( QVariant( double( oValue ) ).toString() );
			pItemList << new QStandardItem( "Double" );
			break;
		}
		case CVAStructValue::STRING:
		{
			pItemList << new QStandardItem( QString( std::string( oValue ).c_str() ) );
			pItemList << new QStandardItem( "String" );
			break;
		}
		case CVAStructValue::DATA:
		{
			pItemList << new QStandardItem( QVariant( int( oValue.GetDataSize() ) ).toString() );
			pItemList << new QStandardItem( "Bytes" );
			break;
		}
		case CVAStructValue::STRUCT:
		{
			BuildStandardItemFromVAStruct( oValue, pItemList.first() );
			break;
		}
		}

		pRootNode->appendRow( pItemList );
	}
};

inline QVariantHash ConvertVAStructToQHash( const CVAStruct& oStruct )
{
	QVariantHash oHash;

	CVAStruct::const_iterator cit = oStruct.Begin();
	while( cit != oStruct.End() )
	{
		const QString sKey( cit->first.c_str() );
		const CVAStructValue& oValue( cit->second );
		cit++;

		switch( oValue.GetDatatype() )
		{
		case CVAStructValue::BOOL:
		{
			oHash[ sKey ] = bool( oValue );
			break;
		}
		case CVAStructValue::INT:
		{
			oHash[ sKey ] = int( oValue );
			break;
		}
		case CVAStructValue::DOUBLE:
		{
			oHash[ sKey ] = double( oValue );
			break;
		}
		case CVAStructValue::STRING:
		{
			oHash[ sKey ] = QString( std::string( oValue ).c_str() );
			break;
		}
		case CVAStructValue::DATA:
		{
			oHash[ sKey ] = QByteArray( ( const char* ) oValue.GetData(), oValue.GetDataSize() );
			break;
		}
		case CVAStructValue::STRUCT:
		{
			oHash[ sKey ] = ConvertVAStructToQHash( oValue );
			break;
		}
		}
	}

	return oHash;
};

inline CVAStruct ConvertQHashToVAStruct( const QVariantHash& oHash )
{
	CVAStruct oStruct;
	
	QVariantHash::const_iterator cit = oHash.constBegin();
	while( cit != oHash.constEnd() )
	{		
		const QString& sKey( cit.key() );
		const QVariant& oValue( cit.value() );
		++cit;

		std::string sKey2 = sKey.toStdString();

		bool bOK = true;
		switch( oValue.type() )
		{
		case QVariant::Bool:
		{
			oStruct[ sKey.toStdString() ] = oValue.toBool();
			break;
		}
		case QVariant::Int:
		{
			oStruct[ sKey.toStdString() ] = oValue.toInt( &bOK );
			break;
		}
		case QVariant::Double:
		{
			oStruct[ sKey.toStdString() ] = oValue.toDouble( &bOK );
			break;
		}
		case QVariant::String:
		{
			oStruct[ sKey.toStdString() ] = oValue.toString().toStdString();
			break;
		}
		case QVariant::ByteArray:
		{
			oStruct[ sKey.toStdString() ] = CVAStructValue( (void*) oValue.toByteArray().data(), oValue.toByteArray().size() );
			break;
		}
		case QVariant::Map:
		case QVariant::Hash:
			oStruct[ sKey.toStdString() ] = ConvertQHashToVAStruct( oValue.toHash() );
			break;
		}
	}

	return oStruct;
};

#endif // IW_REDSTART_UTILS
