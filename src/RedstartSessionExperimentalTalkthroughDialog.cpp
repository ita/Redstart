/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "RedstartSessionExperimentalTalkthroughDialog.h"
#include <ui_RedstartSessionExperimentalTalkthroughDialog.h>

#include <QFileDialog>
#include <QSettings>

RedstartSessionExperimentalTalkthroughDialog::RedstartSessionExperimentalTalkthroughDialog( QWidget *parent )
	: QDialog( parent )
	, ui( new Ui::RedstartSessionExperimentalTalkthroughDialog )
	, m_bStartImmediately( false )
{
	ui->setupUi( this );

	QDialog::setWindowTitle( "Experimental talkthrough session" );

	QSettings qSettings;
	QString sLastBrowseFolder = qSettings.value( "Redstart/last_browse_folder" ).toString();
	std::string s = sLastBrowseFolder.toStdString();

	if( QDir( sLastBrowseFolder ).exists() )
		m_oLastBasePath.setCurrent( sLastBrowseFolder );

	connect( ui->spinBox_num_channels, SIGNAL( valueChanged( int ) ), this, SLOT( NumChannelsChanged( int ) ) );
}

RedstartSessionExperimentalTalkthroughDialog::~RedstartSessionExperimentalTalkthroughDialog()
{
	if( m_oLastBasePath.exists() )
		m_qSettings.setValue( "Redstart/last_browse_folder", m_oLastBasePath.absolutePath() );
	delete ui;
}

QString RedstartSessionExperimentalTalkthroughDialog::GetSessionName() const
{
	return ui->lineEdit_session_name->text();
}

bool RedstartSessionExperimentalTalkthroughDialog::GetStartImmediately() const
{
	return m_bStartImmediately;
}

QVariantHash RedstartSessionExperimentalTalkthroughDialog::GetCoreConfig() const
{
	QVariantHash oFinalCoreConfig;

	QVariantHash oPaths;
	if( m_oDemoSoundBasePath.exists() )
		oPaths[ "DemoSoundBasePath" ] = m_oDemoSoundBasePath.absolutePath();
	if( m_oHRIRBasePath.exists() && m_oDemoSoundBasePath != m_oHRIRBasePath )
		oPaths[ "DefaultHRIRBasePath" ] = m_oHRIRBasePath.absolutePath();

	oFinalCoreConfig[ "Paths" ] = oPaths;


	QString sDemoSoundMacro = ui->lineEdit_macro_DemoSound->text();
	if( ui->checkBox_folders_as_search_path->isChecked() )
	{
		QFileInfo oDemoSound( sDemoSoundMacro );
		sDemoSoundMacro = oDemoSound.fileName();
	}

	QVariantHash oMacros;
	oMacros[ "DefaultHRIR" ] = "$(DefaultHRIR)";
	oMacros[ "DemoSound" ] = sDemoSoundMacro;

	oFinalCoreConfig[ "Macros" ] = oMacros;

	QStringList vsDeviceChannels = ui->lineEdit_output_channels->text().split( ",", QString::SkipEmptyParts );
	QString sDeviceNames;

	int i = 0;
	for( QString sChannelNumber : vsDeviceChannels )
	{
		QVariantHash oDevice;
		oDevice[ "Type" ] = "LS";
		oDevice[ "Channels" ] = sChannelNumber;

		QString sID = "GenericDevice" + sChannelNumber;
		oFinalCoreConfig[ "OutputDevice:" + sID ] = oDevice;
		if( i++ == 0 )
			sDeviceNames = sID;
		else
			sDeviceNames += "," + sID;
	}

	QVariantHash oOutput;
	oOutput[ "Devices" ] = sDeviceNames;

	oFinalCoreConfig[ "Output:MyGenericOutput" ] = oOutput;

	QVariantHash oReproduction;
	oReproduction[ "Class" ] = "Talkthrough";
	oReproduction[ "Outputs" ] = "MyGenericOutput";

	oFinalCoreConfig[ "Reproduction:MyGenericTalkthrough" ] = oReproduction;

	QVariantHash oRenderer;
	oRenderer[ "Class" ] = "PrototypeGenericPath";
	oRenderer[ "NumChannels" ] = ui->spinBox_num_channels->value();
	oRenderer[ "Reproductions" ] = "MyGenericTalkthrough";

	oFinalCoreConfig[ "Renderer:MyExperimentalRenderer" ] = oRenderer;

	return oFinalCoreConfig;
}

void RedstartSessionExperimentalTalkthroughDialog::on_pushButton_CreateSession_clicked()
{
	accept();
	close();
}

void RedstartSessionExperimentalTalkthroughDialog::on_pushButton_CreateAndStartSession_clicked()
{
	m_bStartImmediately = true;
	accept();
	close();
}

void RedstartSessionExperimentalTalkthroughDialog::on_pushButton_select_demo_sound()
{
	QFileDialog fd;
	fd.setNameFilter( "WAV files (*.wav)" );
	fd.setViewMode( QFileDialog::Detail );

	if( m_oLastBasePath.exists() )
		fd.setDirectory( m_oLastBasePath );
	else
		fd.setDirectory( QDir( QApplication::applicationDirPath() ) );

	if( fd.exec() )
	{
		QStringList lFiles = fd.selectedFiles();
		if( lFiles.empty() == false )
		{
			QString sFilePath = lFiles[ 0 ];
			QFile oFile( sFilePath );
			if( oFile.exists() )
			{
				m_oDemoSoundBasePath = fd.directory();
				m_oLastBasePath = m_oDemoSoundBasePath;
			}
			ui->lineEdit_macro_DemoSound->setText( oFile.fileName() );
		}
	}
}

void RedstartSessionExperimentalTalkthroughDialog::NumChannelsChanged( int iNumChannels )
{
	QString sChannelLayout;
	for( int i = 0; i < iNumChannels; i++ )
	{
		if( i > 0 )
			sChannelLayout += ",";
		sChannelLayout += QString::number( i + 1 );
	}

	ui->lineEdit_output_channels->setText( sChannelLayout );
}
