/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "RedstartSessionWizardDialog.h"
#include <ui_RedstartSessionWizardDialog.h>
#include "RedstartUtils.h"

#include <VAException.h>
#include <VAStruct.h>
#include <VACore.h>

#include <QFileDialog>
#include <QSettings>

RedstartSessionWizardDialog::RedstartSessionWizardDialog( QWidget *parent )
	: QDialog( parent ),
	ui( new Ui::RedstartSessionWizardDialog )
{
	ui->setupUi( this );

	QDialog::setWindowTitle( "Session wizard" );

	QSettings qSettings;
	QString sLastBrowseFolder = qSettings.value( "Redstart/last_browse_folder" ).toString();
	std::string s = sLastBrowseFolder.toStdString();

	if( QDir( sLastBrowseFolder ).exists() )
		m_oLastBasePath.setCurrent( sLastBrowseFolder );
}

RedstartSessionWizardDialog::~RedstartSessionWizardDialog()
{
	if( m_oLastBasePath.exists() )
		m_qSettings.setValue( "Redstart/last_browse_folder", m_oLastBasePath.absolutePath() );
	delete ui;
}

QString RedstartSessionWizardDialog::GetSessionName() const
{
	return ui->lineEdit_session_name->text();
}

void RedstartSessionWizardDialog::SetSession( const QString& sSessionName, const QVariantHash& oConfig )
{
	ui->lineEdit_session_name->setText( sSessionName );
	m_oCurrentConfig = oConfig;
}

bool RedstartSessionWizardDialog::GetDuplicationRequested() const
{
	return ui->checkBox_duplicate->isChecked();
}

void RedstartSessionWizardDialog::SetDuplicationOptionDeactivated()
{
	ui->checkBox_duplicate->setChecked( false );
	ui->checkBox_duplicate->setEnabled( false );
}

void RedstartSessionWizardDialog::SetDuplicationOptionActivated()
{
	ui->checkBox_duplicate->setChecked( true );
	ui->checkBox_duplicate->setEnabled( true );
}

QVariantHash RedstartSessionWizardDialog::GetCoreConfig() const
{
	return m_oCurrentConfig;
}

void RedstartSessionWizardDialog::on_pushButton_Save_clicked()
{
	accept();
	close();
}
