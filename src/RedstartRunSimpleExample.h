/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_RUN_SIMPLE_EXAMPLE
#define IW_REDSTART_RUN_SIMPLE_EXAMPLE

#include <VA.h>

inline void RunSimpleExample( IVAInterface* pVA )
{
	const std::string sSignalSourceID = pVA->CreateSignalSourceBufferFromFile( "$(DemoSound)" );
	pVA->SetSignalSourceBufferPlaybackAction( sSignalSourceID, IVAInterface::VA_PLAYBACK_ACTION_PLAY );
	pVA->SetSignalSourceBufferLooping( sSignalSourceID, true );

	const int iSoundSourceID = pVA->CreateSoundSource( "Redstart C++ example sound source" );
	pVA->SetSoundSourcePose( iSoundSourceID, VAVec3( 2.0f, 1.7f, 2.0f ), VAQuat( 0.0f, 0.0f, 0.0f, 1.0f ) );

	pVA->SetSoundSourceSignalSource( iSoundSourceID, sSignalSourceID );

	const int iHRIR = pVA->CreateDirectivityFromFile( "$(DefaultHRIR)" );

	const int iSoundReceiverID = pVA->CreateSoundReceiver( "Redstart C++ example sound receiver" );
	pVA->SetSoundReceiverPose( iSoundReceiverID, VAVec3( 0.0f, 1.7f, 0.0f ), VAQuat( 0.0f, 0.0f, 0.0f, 1.0f ) );
	pVA->SetSoundReceiverDirectivity( iSoundReceiverID, iHRIR );
};

#endif // IW_REDSTART_RUN_SIMPLE_EXAMPLE
