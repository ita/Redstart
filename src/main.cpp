/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include <QApplication>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QLocale>

#include "RedstartWindow.h"

int main( int argc, char* argv[] )
{
	// Qt app
	QApplication a( argc, argv );
	a.setOrganizationName( "Institute of Technical Acoustics (ITA)" );
	a.setOrganizationDomain( "akustik.rwth-aachen.de" );
	a.setApplicationName( "Redstart" );
	a.setApplicationDisplayName( "Redstart VA GUI [BETA]" );

	QLocale::setDefault( QLocale::English );

	QIcon icon;
	icon.addFile( QStringLiteral( ":/banners/logo/BlackRedstartM.png" ), QSize(), QIcon::Normal, QIcon::On );
	a.setWindowIcon( icon );

	QCommandLineParser oCLIParser;
	oCLIParser.setApplicationDescription( "Redstart helper" );
	oCLIParser.addHelpOption();
	oCLIParser.addVersionOption();
	oCLIParser.addPositionalArgument( "session", QCoreApplication::translate( "main", "Redstart session or VA core configuration file path" ) );

	QCommandLineOption bAutoStartVA( "a", QCoreApplication::translate( "main", "Autostart VA core" ) );
	oCLIParser.addOption( bAutoStartVA );

	QCommandLineOption oSafeModeOption( QStringList() << "s" << "safemode", QCoreApplication::translate( "main", "Run in safemode (override and disable autostart)" ) );
	oCLIParser.addOption( oSafeModeOption );

	oCLIParser.process( a );

	bool bFailSafeMode = oCLIParser.isSet( oSafeModeOption );
	bool bAutoStart = oCLIParser.isSet( bAutoStartVA );

	RedstartWindow w( bFailSafeMode, bAutoStart );
	w.show();

	return a.exec();
};
