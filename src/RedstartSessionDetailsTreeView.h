/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_SESSION_DETAILS_TREE_VIEW
#define IW_REDSTART_SESSION_DETAILS_TREE_VIEW

#include <QStandardItemModel>
#include <QStandardItem>
#include <QTreeView>
#include "RedstartUtils.h"
#include <VAStruct.h>

class RedstartSessionDetailModel : public QStandardItemModel
{};

class RedstartSessionDetailsTreeView : public QTreeView
{
	Q_OBJECT

public:
	inline RedstartSessionDetailsTreeView( QWidget* pParent )
		: QTreeView( pParent )
		, m_pModel( NULL )
	{
		m_pModel = new RedstartSessionDetailModel();
		this->setModel( m_pModel );
		SetDefaults();
		
		setEditTriggers( QAbstractItemView::NoEditTriggers );
		//setSelectionMode( QAbstractItemView::SingleSelection );
	};

	inline void SetDefaults()
	{
		m_pModel->setHorizontalHeaderLabels( QStringList() << "Key" << "Value" << "Type" );
	};

	void SetStruct( const CVAStruct& oStruct )
	{
		m_pModel->clear();
		SetDefaults();

		BuildStandardItemFromVAStruct( oStruct, m_pModel->invisibleRootItem() );

		collapseAll();
		resizeColumnToContents( 0 );
		resizeColumnToContents( 1 );
		resizeColumnToContents( 2 );
	};

	inline ~RedstartSessionDetailsTreeView() {};

	private slots:

private:
	RedstartSessionDetailModel* m_pModel;
};

#endif // IW_REDSTART_SESSION_DETAILS_TREE_VIEW
