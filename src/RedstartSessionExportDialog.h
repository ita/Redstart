/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2021
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_SESSION_EXPORT_DIALOG
#define IW_REDSTART_SESSION_EXPORT_DIALOG

#include <QDialog>
#include <QDir>
#include <QFileInfo>
#include <QString>
#include <QSettings>
#include <QHash>

namespace Ui
{
	class RedstartSessionExportDialog;
}

class RedstartSessionExportDialog : public QDialog
{
	Q_OBJECT

public:
	explicit RedstartSessionExportDialog( QWidget *parent );
	~RedstartSessionExportDialog();
	QVariantHash GetCoreConfig() const;
	QString GetSessionName() const;
	bool ValidFile() const;
	bool GetStartImmediately() const;

	private slots:
	void on_pushButton_CreateSession_clicked();
	void on_pushButton_CreateAndStartSession_clicked();
	void on_pushButton_select_import_file_clicked();

private:
	Ui::RedstartSessionExportDialog* ui;
	QDir m_oLastBasePath;
	QFileInfo m_oConfigFileBasePath;
	QSettings m_qSettings;
	bool m_bStartImmediately;
};

#endif // IW_REDSTART_SESSION_EXPORT_DIALOG
