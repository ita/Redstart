/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2017
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */
#ifndef IW_REDSTART_DEFAULT_CORE_CONFIG
#define IW_REDSTART_DEFAULT_CORE_CONFIG

#include <QHash>
#include <VAStruct.h>

inline QVariantHash DefaultMacroSection()
{
	QVariantHash oHash;
	oHash[ "ProjectName" ] = "MyRedstartProject";
	return oHash;
};

inline QVariantHash DefaultDebugSection()
{
	QVariantHash oHash;
	oHash[ "OutputRecordEnabled" ] = false;
	oHash[ "OutputRecordFilePath" ] = "$(ProjectName)_out.wav";
	oHash[ "LogLevel" ] = 2;
	return oHash;
};

inline QVariantHash DefaultCalibrationSection()
{
	QVariantHash oHash;
	oHash[ "DefaultSoundSpeed" ] = 344.0;
	oHash[ "DefaultStaticPressure" ] = 101125.0;
	oHash[ "DefaultTemperature" ] = 20.0;
	oHash[ "DefaultRelativeHumidity" ] = 20.0;
	//oHash[ "DefaultShiftSpeed" ] = QVariantList( 0.0, 0.0, 0.0 );
	return oHash;
};

inline QVariantHash DefaultHeadphoneSetup( const QString& sChannels = "1,2" )
{
	QVariantHash oSetup;

	QVariantHash oDevice;
	oDevice[ "Type" ] = "HP";
	oDevice[ "Channels" ] = sChannels;

	oSetup[ "OutputDevice:MyHP" ] = oDevice;

	QVariantHash oOutput;
	oOutput[ "Devices" ] = "MyHP";

	oSetup[ "Output:MyDesktopHP" ] = oOutput;

	return oSetup;
};

#endif // IW_REDSTART_DEFAULT_CORE_CONFIG