## Redstart

Redstart is a graphical C++ client or server application for real-time auralization using Virtual Acoustics (VA) and Qt.

### License

Copyright 2015-2020 Institute of Technical Acoustics (ITA), RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Quick build guide

#### Qt Creator and Windows platform

The Qt Creator requires the paths to all dependent libraries, but they are not automatically added to the environment. Otherwise, the program execution will immediately crash without any further useful information (the reason is: it can't locate runtime libraries).
 
A workaround:
1. Copy the `platform` folder from the Qt binaries into the `bin` folder of the project, usually a cryptic name like `build-Redstart-Desktop_Qt_5_6_0_MSVC2013_64bit-Release` or similar.
2. Deploy VA with Visual Studio using the INSTALL directive. It will assemble binaries of the Redstart application and you will find a folder called something like `dist/win32-x64.vc12/bin`. Add this directory as PATH entry in Qt Creator > Project in the Build Environment section.

Do not forget to Build & Install Redstart/VA in Visual Studio in case you modify the core! Copying DLLs is always dangerous during development ...

Now you can run the program from within Qt Creator. Repeat this for every kit you want to use.

#### Visual Studio and Windows platform

You can edit the UI with Qt Designer, but Visual Studio may fail to generate / modify the source code automatically, because modification detection is not always picking up changes. In this case it is sometimes necessary to clean up the intermediate source files in the build folder. But generally, VS is able to run Qt specific compiler calls prior to compiling the application, cmake and qmake take care of this.
